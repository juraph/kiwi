#!/usr/bin/env python3

from build123d import *

batt_width = 34 # mm
batt_length = 131 # mm
batt_thickness = 18 # mm

with BuildPart() as battery:
    with BuildSketch() as plan:
        Rectangle(batt_width, batt_length)
    extrude(amount=batt_thickness)

    batt_face = battery.faces().sort_by(Axis.Y)[0]
    RigidJoint(
        "batt_mount",
        battery.part,
        Location((batt_face.center()), (90, 0, 0)),
    )


# Render the part
if "show_object" in locals():
    show_object(battery)
    show_object(battery.part.joints["batt_mount"].symbol)
