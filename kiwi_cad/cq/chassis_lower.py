#!/usr/bin/env python3

from logging import shutdown
from build123d import *
# Import the upper chassis, to use its dimensions
import chassis_upper
import as5048_mount
import imu

lower_connecting_height = 30

with BuildPart() as l_chassis:

    Box(chassis_upper.backplate_width, lower_connecting_height, chassis_upper.backplate_thickness)
    # Grab upper face, which is mounted to the upper chassis plate
    top_face = l_chassis.faces().sort_by(Axis.Y)[0]

    # Also store left/right faces, which will be used later
    encoder_mount_base_faces = [l_chassis.faces().sort_by(Axis.X)[0],  l_chassis.faces().sort_by(Axis.X)[-1]]

    # Render the connectors to the 8108 BLDCs;
    bottom_face = l_chassis.faces().sort_by(Axis.Y)[-1]

    # Grab the edges of the bottom face, used to place mounts at edges
    far_edge : Edge = bottom_face.edges().sort_by(Axis.X)[0]

    edge_locations = ((0, chassis_upper.backplate_width/2, 0), (0, -chassis_upper.backplate_width/2, 0))

    # Motor mounting extrusions
    with BuildSketch(bottom_face):
        with Locations(*edge_locations):
            Rectangle(chassis_upper.backplate_thickness, 15)
    extrude(amount=60)

    o_e_1 = l_chassis.edges().filter_by(Axis.Z).sort_by(Axis.Y)[0:8].sort_by(Axis.X)[0]
    o_e_2 = l_chassis.edges().filter_by(Axis.Z).sort_by(Axis.Y)[0:8].sort_by(Axis.X)[-1]


    left_motor_face = l_chassis.faces().sort_by(Axis.X)[-1]
    right_motor_face = l_chassis.faces().sort_by(Axis.X)[0]

    # Select inner faces of motor braces, these are used later for the encoder mounts
    inner_faces = l_chassis.faces(select=Select.LAST).filter_by(Axis.X)[1:3]
    outer_faces = [l_chassis.faces(select=Select.LAST).filter_by(Axis.X)[0], l_chassis.faces(select=Select.LAST).filter_by(Axis.X)[-1]]

    # Place screw holes for the motors
    for face in outer_faces:
        with BuildSketch(face) as t_face:
            sgn = 1 if t_face.workplanes[0].origin.X < 0 else -1
            with Locations((0, 10 * sgn, 0)):
                Circle(radius=9)
                with PolarLocations(16, 4, start_angle=45):
                    Circle(radius=2)
    extrude(amount=-40, mode=Mode.SUBTRACT)

    for face in inner_faces:
        with BuildSketch(face) as t_face:
            sgn = 1 if t_face.workplanes[0].origin.X < 0 else -1
            with Locations((0, 10 * -sgn, 0)):
                with PolarLocations(16, 4, start_angle=45):
                    Circle(radius=3)
    extrude(amount=-2, mode=Mode.SUBTRACT)


    # TODO Make these thinner
    for face in encoder_mount_base_faces:
        t =  [face.edges().sort_by(Axis.Y)[0], face.edges().sort_by(Axis.Y)[-1]]
        face_height = t[1].position_at(1).Y - t[0].position_at(1).Y
        with BuildSketch(face) as t_face:
            sgn = 1 if t_face.workplanes[0].origin.X < 0 else -1
            with Locations((0, sgn * (10/2 - face_height/2), 0)):
                Rectangle(30, 10)
    extrude(amount=50)
    new_edges = l_chassis.edges(select=Select.LAST).filter_by_position(Axis.X, minimum=-40, maximum=40).filter_by_position(Axis.Z, minimum=-15, maximum=15)
    new_faces = l_chassis.faces(select=Select.LAST).filter_by(Axis.Y).sort_by(Axis.Z)[:2]

    fillet(objects=new_edges, radius=3)
    # TODO Make these thinner
    for face in new_faces:
        t =  [face.edges().filter_by(Axis.Z)[0], face.edges().filter_by(Axis.Z)[-1]]
        face_width = t[1].position_at(1).X - t[0].position_at(1).X
        with BuildSketch(face) as t_face:
            sgn = 1 if t_face.workplanes[0].origin.X < 0 else -1
            with Locations((0, sgn * (10/2 - 25), 0)):
                Rectangle(30, 10)
    extrude(amount=67.5)

    outer_enc_faces = [l_chassis.faces(Select.LAST).sort_by(Axis.X)[-3], l_chassis.faces(Select.LAST).sort_by(Axis.X)[2]]

    # Place encoder mounts (Outdated, going to be placed outside wheel)
    for face in outer_enc_faces:
        t =  [face.edges().filter_by(Axis.Z)[0], face.edges().filter_by(Axis.Z)[-1]]
        face_width = t[1].position_at(1).X - t[0].position_at(1).X
        with BuildSketch(face) as t_face:
            sgn = 1 if t_face.workplanes[0].origin.X < 0 else -1
            with Locations((0, sgn * (5.0 - 25), 0)):
                Rectangle(25, 30)
    extrude(amount=-5, mode=Mode.SUBTRACT)

    l_enc_mount_face = l_chassis.faces(select=Select.LAST).sort_by(Axis.X)[1]
    r_enc_mount_face = l_chassis.faces(select=Select.LAST).sort_by(Axis.X)[-2]

    # TODO Bolt holes for the as5048 encoders
    outer_faces = [l_chassis.faces().sort_by(Axis.X)[i] for i in (0, -1)]
    for face in outer_faces:
        sgn = 1 if face.center().X < 0 else -1
        with BuildSketch(face):
            with Locations((0, 23 * sgn, 0)):
                with Locations((as5048_mount.lower_hole_hor_loc, sgn * as5048_mount.lower_hole_vert_loc)):
                    Circle(1.3)
                with Locations((-as5048_mount.lower_hole_hor_loc, sgn * as5048_mount.lower_hole_vert_loc)):
                    Circle(1.3)
                with Locations((-as5048_mount.lower_hole_hor_loc, sgn * as5048_mount.lower_hole_vert_loc - sgn * 11)):
                    Circle(1.3)
                with Locations((as5048_mount.lower_hole_hor_loc, sgn * as5048_mount.lower_hole_vert_loc - sgn * 11)):
                    Circle(1.3)
    extrude(amount=-10, mode=Mode.SUBTRACT)


    # Get to filleting
    # Fancy Chamfers
    motor_brace_edges = (bottom_face.edges().sort_by(Axis.X)[0], bottom_face.edges().sort_by(Axis.X)[-1])
    fillet(objects=motor_brace_edges, radius=3)

    motor_brace_edges = (inner_faces[0].edges().sort_by(Axis.Z)[1], inner_faces[1].edges().sort_by(Axis.Z)[1])
    fillet(motor_brace_edges, 1)


    # fillet lowest faces
    lowest_faces = l_chassis.edges().filter_by(Axis.X).sort_by(Axis.Y)[-5:]
    # fillet(objects=lowest_faces, radius=10)

    # fillet(o_e_1, radius=2)
    # fillet(o_e_2, radius=2)

    n_e_1 = l_chassis.edges().filter_by(Axis.Z).sort_by(Axis.X)[-1]
    n_e_2 = l_chassis.edges().filter_by(Axis.Z).sort_by(Axis.X)[1]

    # l_chassis.max_fillet(n_e_1)
    # l_chassis.max_fillet(n_e_2)

    # Create brace
    back_face = l_chassis.faces().sort_by(Axis.Z)[-1]
    with BuildSketch(back_face) as ax:
        with Locations(*((27.5, 39.3, 0), (-27.5, 39.3, 0))):
           Rectangle(10,10)
    extrude(amount=60)
    last_faces = l_chassis.faces(select=Select.LAST)
    t_faces = [last_faces.sort_by(Axis.X)[0], last_faces.sort_by(Axis.X)[-1]]

    # Perform fillets here, while it's convenient
    brace_fillet_1 = [l_chassis.edges(select=Select.LAST).filter_by(Axis.Y).filter_by_position(Axis.Z, minimum=0, maximum=35).sort_by(Axis.X)[i] for i in (0, -1)]
    brace_fillet_2 = l_chassis.edges(select=Select.LAST).filter_by(Axis.X).filter_by_position(Axis.Z, minimum=0, maximum=35).sort_by(Axis.Y)[0:2]
    fillet(objects=brace_fillet_1, radius=4.5)
    brace_fillet_2 = l_chassis.edges(select=Select.LAST).filter_by(Axis.X).filter_by_position(Axis.Z, minimum=0, maximum=35).sort_by(Axis.Y)
    # fillet(objects=brace_fillet_2, radius=2.0)

    for face in t_faces:
        with BuildSketch(face) as ax:
            sgn = 1 if ax.workplanes[0].origin.X < 0 else -1
            with Locations((0, -sgn * 25,0)):
                Rectangle(10,10)
    extrude(amount=47.5)

    last_faces = l_chassis.faces(select=Select.LAST).filter_by(Axis.Z)
    t_faces = [last_faces.sort_by(Axis.Z)[0], last_faces.sort_by(Axis.Z)[1]]
    for face in t_faces:
        with BuildSketch(face) as ax:
            sgn = 1 if ax.workplanes[0].origin.X < 0 else -1
            with Locations((0, -sgn * 18.75, 0)):
                Rectangle(10,10)
    extrude(amount=55)

    # More filleting
    brace_top_edges = l_chassis.edges(select=Select.LAST).filter_by(Axis.X).filter_by_position(Axis.Z, minimum=0, maximum=30).sort_by(Axis.Y)[0:2]

    # fillet(objects=brace_top_edges, radius=15.0)

    # fillet the edges around the brace
    outer_brace_edges = l_chassis.edges().filter_by(Axis.Y).filter_by_position(Axis.Z, minimum=80, maximum=400)
    inner_brace_edges = l_chassis.edges().filter_by(Axis.Y).filter_by_position(Axis.Z, minimum=40, maximum=80)

    fillet(objects=outer_brace_edges, radius=10.0)
    fillet(objects=inner_brace_edges, radius=4.0)

    # Create mount for the IMU
    with BuildSketch(bottom_face):
        Rectangle(25, 25)
    extrude(amount=5)
    # Fancy fillets
    imu_face = l_chassis.faces(Select.LAST).sort_by(Axis.Y)[-1]
    t_edges = l_chassis.edges(Select.LAST).filter_by_position(Axis.Y, minimum=0, maximum=17).filter_by_position(Axis.Z, minimum=-15, maximum=15)

    imu_lower_hole_hor_loc = 25/2 - 2
    with BuildSketch(imu_face):
       with Locations((imu.lower_hole_hor_loc, imu.lower_hole_vert_loc)):
           Circle(1.3)
       with Locations((-imu.lower_hole_hor_loc, imu.lower_hole_vert_loc)):
           Circle(1.3)
       with Locations((-imu.lower_hole_hor_loc, -imu.lower_hole_vert_loc)):
           Circle(1.3)
       with Locations((imu.lower_hole_hor_loc, -imu.lower_hole_vert_loc )):
           Circle(1.3)
    extrude(amount=-10, mode=Mode.SUBTRACT)


    fillet(objects=t_edges, radius=2.0)


    RigidJoint("upper_mount", l_chassis.part,
        Location((top_face.center()), (0, 180, -90)),
    )

    RigidJoint("l_m_mount", l_chassis.part,
        Location((left_motor_face.center().X, left_motor_face.center().Y + 10, left_motor_face.center().Z), (0,90,0)),
    )

    RigidJoint("r_m_mount", l_chassis.part,
        Location((right_motor_face.center().X, right_motor_face.center().Y + 10, right_motor_face.center().Z), (0,-90,0)),
    )

    RigidJoint("l_enc_mount", l_chassis.part,
        Location((l_enc_mount_face.center()), (0,90,0)),
    )

    RigidJoint("r_enc_mount", l_chassis.part,
        Location((r_enc_mount_face.center()), (0,90,0)),
    )


if "show_object" in locals():
    show_object(l_chassis)

    # imu = imu.imu
    # show_object(imu)
    # show_object(as5048_mount.as5048)
    # show_object(l_chassis.part.joints["upper_mount"].symbol)
    # show_object(l_chassis.part.joints["l_m_mount"].symbol)
    # show_object(l_chassis.part.joints["r_m_mount"].symbol)
    # show_object(l_chassis.part.joints["l_enc_mount"].symbol)
    # show_object(l_chassis.part.joints["r_enc_mount"].symbol)
    pass
