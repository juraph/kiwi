#!/usr/bin/env python3

from build123d import *

sensor_centerpoint_x = 22/2
sensor_centerpoint_y = 1.4 + 11/2 # Measured from top

lower_hole_vert_loc = 14 - 2
lower_hole_hor_loc = 11 - 2

with BuildPart() as as5048:
   Box(22, 28, 2.0)
   top_face = as5048.faces().sort_by()[0]
   with BuildSketch(top_face):
       with Locations((lower_hole_hor_loc, lower_hole_vert_loc)):
           Circle(1.3)
       with Locations((-lower_hole_hor_loc, lower_hole_vert_loc)):
           Circle(1.3)
       with Locations((-lower_hole_hor_loc, lower_hole_vert_loc - 11)):
           Circle(1.3)
       with Locations((lower_hole_hor_loc, lower_hole_vert_loc - 11)):
           Circle(1.3)
   extrude(amount=-100, mode=Mode.SUBTRACT)


   RigidJoint("mount", as5048.part,
        Location((top_face.center()), (0, 180, 180)),
   )


if 'show_object' in locals():
    show_object(as5048)
    show_object(as5048.part.joints["mount"].symbol)
