#!/usr/bin/env python3

from build123d import *

odrive: Compound = import_step("../static_meshes/ODrive-3-5.step")


odrive_h_x_f:Face = odrive.faces().sort_by(Axis.X)[0]
odrive_l_x_f:Face = odrive.faces().sort_by(Axis.X)[-1]

odrive_h_y_f:Face = odrive.faces().sort_by(Axis.Y)[0]
odrive_l_y_f:Face = odrive.faces().sort_by(Axis.Y)[-1]

odrive_j_x = odrive_l_x_f.center().X - odrive_h_x_f.center().X
odrive_j_y = odrive_l_y_f.center().Y - odrive_h_y_f.center().Y

# Baseplate length = 142
# Extented length (Including the bolt mounts) = 156

# The odrive is mounted on a ally plate. The dimensions of which are
# longer than the odrive itself
with BuildPart() as odrive_baseplate:
    with Locations((odrive_j_x/2, odrive_j_y/2, 3)):
        Box(142, 50, 3)

    # Grab the Top and Bottom faces
    edges_faces = (odrive_baseplate.faces().sort_by(Axis.X)[0], odrive_baseplate.faces().sort_by(Axis.X)[-1])
    with BuildPart(*edges_faces):
        with Locations((0, -25 + 4, 3.5), (0, 25 - 4, 3.5)):
            Box(3,8, 7)

    # Grab the edges on the far edges, for chamfering
    vert_edges = odrive_baseplate.edges().filter_by(Axis.Z)
    chamf_edges_1 = vert_edges.sort_by(Axis.X)[:4]
    chamf_edges_2 = vert_edges.sort_by(Axis.X)[-4:]
    fillet(objects=chamf_edges_1, radius=2.0)
    fillet(objects=chamf_edges_2, radius=2.0)

odrive_assy = Compound(label="odrive_assy", children=[odrive, odrive_baseplate.part])

RigidJoint(
    "odrive_mount",
    odrive_assy,
    Location((odrive_j_x/2, odrive_j_y/2, 5), (0, 0, 0)),
)


if 'show_object' in locals():
    show_object(odrive_assy)
    show_object(odrive_assy.joints["odrive_mount"].symbol)
