#!/usr/bin/env python3

import math
import copy
from build123d import *

outer_diam = 86.8 #mm
outer_radius = outer_diam/2 #mm
inner_radius = outer_radius - 2
center_thickness = 12 #mm
top_shaft_hole_radius = 9 # mm
bottom_shaft_hole_radius = 12 # mm

pole_count = 40

top_plate_pts = [
    (0, center_thickness + 5),
    (-25, center_thickness + 5),
    (-outer_radius, center_thickness + 2),
    (-outer_radius, center_thickness),
]

bottom_plate_pts = [
    (0, center_thickness + 5),
    (-25, center_thickness + 5),
    (-outer_radius, center_thickness + 2),
    (-outer_radius, center_thickness),
]

with BuildPart() as motor:
    with BuildSketch() as plan:
        Circle(radius=outer_radius)
    extrude(amount=center_thickness)
    top_face = motor.faces().sort_by(Axis.Z)[-1]
    with BuildSketch(top_face) as plan:
        Circle(radius=inner_radius)
    extrude(amount=-center_thickness, mode=Mode.SUBTRACT)
    # Draw magnets
    with BuildSketch() as plan:
        with PolarLocations(inner_radius, 40):
                with Locations((0,0)):
                    Rectangle(2,3)
    extrude(amount=center_thickness - 1)
    # Draw upper section
    with BuildSketch(Plane.YZ) as plane:
        with BuildLine() as lines:
            l1 = Polyline(*top_plate_pts)
            l2 = Line(l1 @ 1, l1 @ 0)
        make_face()
    revolve(axis=Axis.Z)

    # Cut center hole
    with BuildSketch() as plan:
        Circle(radius=top_shaft_hole_radius)
    extrude(amount=center_thickness * 2, mode=Mode.SUBTRACT)

    target_face : Face = motor.faces().sort_by(Axis.Z)[-1]
    with BuildSketch(target_face):
        locs = PolarLocations(outer_radius, 4)
        for i, j in enumerate(locs):
            temp = copy.deepcopy(j)
            temp.position = Vector(j.position.X, j.position.Y, 2)
            with Locations((temp)):
                with BuildSketch() as bs:
                    poly = Ellipse(x_radius=15, y_radius=22, rotation=90)
                    test_edge = poly.edges()

        locs = PolarLocations(outer_radius, count=4, start_angle=45)
        for i, j in enumerate(locs):
            temp = copy.deepcopy(j)
            temp.position = Vector(j.position.X, j.position.Y, 2)
            with Locations((temp)):
                with BuildSketch() as bs:
                    poly = Ellipse(x_radius=23, y_radius=9, rotation=0)
                    test_edge = poly.edges()
    extrude(amount=-8, mode=Mode.SUBTRACT)
    lower_section_radius = 29
    lower_section_radius_outer = 34

    with BuildSketch(target_face.offset(-10.0/2)) as plan:
        Circle(radius=lower_section_radius_outer)

    extrude(amount=-center_thickness)
    # Select last face, to use for rotors
    rotor_face = motor.faces(Select.LAST).sort_by(Axis.X)[-1]

    with BuildSketch(target_face.offset(-10.0/2)) as plan:
        Circle(radius=lower_section_radius)
    extrude(amount=-center_thickness, mode=Mode.SUBTRACT)

    # Draw center bearing
    with BuildSketch(target_face) as plan:
        Circle(radius=bottom_shaft_hole_radius)
    extrude(amount=-22)
    with BuildSketch(target_face):
        with BuildSketch() as plan:
            Circle(radius=top_shaft_hole_radius-1)
    extrude(amount=-22, mode=Mode.SUBTRACT)

    bottom_face = motor.faces().sort_by(Axis.Z)[0]

    stator_count = 37
    locs = PolarLocations(outer_radius, count=4)
    for i in range(0, 360, int(360 / stator_count)):
        t_face = bottom_face.offset(-22 + center_thickness)
        pln = Plane(t_face).rotated((90, 90 + i, 0)).offset(lower_section_radius_outer)
        with BuildSketch(pln) as bs:
            Circle(radius=2.75)
    extrude(amount=5.75)

    bottom_revolve_plane = Plane((0, 0, 11), (0, 1, 0), (-1, 0, 0))
    with BuildSketch(bottom_revolve_plane) as plane:
        with BuildLine() as lines:
            l1 = Polyline(*bottom_plate_pts)
            l2 = Line(l1 @ 1, l1 @ 0)
        t = make_face()
    revolve(axis=Axis.Z)

    # Cut center hole
    with BuildSketch(bottom_face) as plan:
        Circle(radius=top_shaft_hole_radius)
    extrude(amount=center_thickness * 2, mode=Mode.SUBTRACT)

    # Cut center hole
    with BuildSketch(bottom_face) as plan:
        Circle(radius=top_shaft_hole_radius)
    extrude(amount=center_thickness * 2, mode=Mode.SUBTRACT)


    new_bottom_face = motor.faces().sort_by(Axis.Z)[0]

    # Draw bottom face
    with BuildSketch(new_bottom_face):
        locs = PolarLocations(outer_radius, 4)
        for i, j in enumerate(locs):
            temp = copy.deepcopy(j)
            temp.position = Vector(j.position.X, j.position.Y, 2)
            with Locations((temp)):
                with BuildSketch() as bs:
                    poly = Ellipse(x_radius=15, y_radius=24, rotation=90)
                    test_edge = poly.edges()

        locs = PolarLocations(outer_radius, count=4, start_angle=45)
        for i, j in enumerate(locs):
            temp = copy.deepcopy(j)
            temp.position = Vector(j.position.X, j.position.Y, 2)
            with Locations((temp)):
                with BuildSketch() as bs:
                    poly = Ellipse(x_radius=23, y_radius=11, rotation=0)
                    test_edge = poly.edges()
    extrude(amount=-5.2, mode=Mode.SUBTRACT)

    input_mount_face : Face = motor.faces().sort_by(Axis.Z)[-1]
    output_mount_face : Face = motor.faces().sort_by(Axis.Z)[0]

    # Finally, add the attatchment points for the motor
    RigidJoint(
        "input_mount",
        motor.part,
        Location((input_mount_face.center()), (0, 180, 0)),
    )

    RigidJoint(
        "output_mount",
        motor.part,
        Location((output_mount_face.center()), (0, 180, 0)),
    )


# Render the part
if "show_object" in locals():
    show_object(motor, options={"alpha":0.0, "color": (50, 50, 50)})
    show_object(motor.part.joints["input_mount"].symbol)
    show_object(motor.part.joints["output_mount"].symbol)
