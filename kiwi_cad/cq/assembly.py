#!/usr/bin/env python3
# import cadquery as cq
from build123d import *
from build123d import copy
from bldc_wheel import wheel
from chassis_upper import u_chassis
from chassis_lower import l_chassis
from raspi import rpi
from battery import battery
from odrive import odrive_assy
from bldc_8108 import motor
from as5048_mount import as5048
from ocp_vscode import *

svg_opts = {
    "width": 400,
    # "height": 300,
    "pixel_scale": 2,
    # "margin_left": 10,
    # "margin_top": 10,
    "show_axes": False,
    "show_hidden": True,
}

# from cq_server.ui import UI, show_object # Used for cq server
kiwi_chassis = u_chassis.part
lower_chassis = l_chassis.part
battery = battery.part
odrive = odrive_assy

left_motor = copy.copy(motor.part)
right_motor = copy.copy(motor.part)

left_wheel= copy.copy(wheel.part)
right_wheel= copy.copy(wheel.part)

left_as5048 = copy.copy(as5048.part)
right_as5048 = copy.copy(as5048.part)


kiwi_chassis.joints["rpi_mount"].connect_to(rpi.joints["rpi_mount"])
kiwi_chassis.joints["batt_mount"].connect_to(battery.joints["batt_mount"])
kiwi_chassis.joints["odrive_mount"].connect_to(odrive.joints["odrive_mount"])
kiwi_chassis.joints["lower_mount"].connect_to(lower_chassis.joints["upper_mount"])

lower_chassis.joints["l_m_mount"].connect_to(left_motor.joints["input_mount"])
lower_chassis.joints["r_m_mount"].connect_to(right_motor.joints["input_mount"])
left_motor.joints["output_mount"].connect_to(left_wheel.joints["axle_mount"])
right_motor.joints["output_mount"].connect_to(right_wheel.joints["axle_mount"])

lower_chassis.joints["l_enc_mount"].connect_to(left_as5048.joints["mount"])
lower_chassis.joints["r_enc_mount"].connect_to(right_as5048.joints["mount"])

#mount the rpi
# kiwi_chassis.joints["rpi_mount"].connect_to(rpi.combined_center())

# Units are in mm
# rpi.faces("<Z").end().center(3.5, 3.5).circle(2.0).tag("left_screwhole")
# # l_8318 = cq.importers.importStep("../static_meshes/8318.STEP")
assembly = Compound(label="assembly", children=[
    kiwi_chassis,
    lower_chassis,
    rpi,
    battery,
    odrive,

    left_motor,
    right_motor,

    left_wheel,
    right_wheel,

    left_as5048,
    right_as5048
], color=Color("blue"))

# SVG export disabled for now
# assembly.export_svg("front.svg",
#                     (200, 100, -200),
#                     (0, -1, 0),
#                     svg_opts=svg_opts)

# assembly.export_svg("rear.svg",
#                     (200, 100, 200),
#                     (0, -1, 0),
#                     svg_opts=svg_opts)

show(assembly)

# if 'show_object' in locals():
#     show_object(assembly)
