#!/usr/bin/env python3

from build123d import *

rpi: Compound = import_step("../static_meshes/rpi.step")

rpi_h_x_f:Face = rpi.faces().sort_by(Axis.X)[0]
rpi_l_x_f:Face = rpi.faces().sort_by(Axis.X)[-1]

rpi_h_y_f:Face = rpi.faces().sort_by(Axis.Y)[0]
rpi_l_y_f:Face = rpi.faces().sort_by(Axis.Y)[-1]

rpi_j_x = rpi_l_x_f.center().X - rpi_h_x_f.center().X
rpi_j_y = rpi_l_y_f.center().Y - rpi_h_y_f.center().Y

RigidJoint(
    "rpi_mount",
    rpi,
    Location((rpi_j_x/2, rpi_j_y/2, -3), (0, 0, 0)),
)

if 'show_object' in locals():
    show_object(rpi)
    show_object(rpi.joints["rpi_mount"].symbol)
