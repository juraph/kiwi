#!/usr/bin/env python3
#
from build123d import *
from ocp_vscode import *


# import cadquery as cq
# from cadquery import selectors
from math import cos, sin, pi, radians, degrees
# from cq_server.ui import UI, show_object

# Motor diameter is 95mm
motor_diameter = 95

outer_wheel_radius = motor_diameter / 2 + 5.0
inner_wheel_radius = outer_wheel_radius - 5.0
wheel_depth = 23
back_plate_depth = 5.0
hole_spacing = 31.0
center_hole_radius = 5
screw_hole_radius = 2

cut_radius = 34

with BuildPart() as wheel:
    # Draw shell
    with BuildSketch() as plane:
        Circle(radius=outer_wheel_radius)
    extrude(amount=wheel_depth)
    fillet_edge = wheel.edges(select=Select.LAST).sort_by(Axis.Z)[0]
    offset(amount=5.0, openings=wheel.faces().sort_by(Axis.Z)[-1], kind=Kind.INTERSECTION)
    new_edges = wheel.edges().sort_by(Axis.Z)[1]

    top_face = wheel.faces().sort_by(Axis.Z)[-1]
    # Apply ridges to wheels, to help give traction
    with BuildSketch(top_face):
        with PolarLocations(outer_wheel_radius + 4.0, 50):
            Circle(radius=2.0)
    extrude(amount=-wheel_depth-5.0)

    # Screw/axle holes
    with BuildSketch(wheel.faces().sort_by(Axis.Z)[0]):
        Circle(radius=center_hole_radius)
        with PolarLocations(radius=cut_radius, count=4, start_angle=45):
            Circle(radius=16)

    extrude(amount=-(back_plate_depth), mode=Mode.SUBTRACT)

    with Locations((wheel.faces().sort_by(Axis.Z)[0])):
        with PolarLocations(radius=hole_spacing/2, count=4):
            CounterBoreHole(radius=screw_hole_radius, counter_bore_radius=screw_hole_radius+2, counter_bore_depth=3)


    z_faces = wheel.faces().filter_by_position(Axis.Z, minimum=0, maximum=10)

    fillet(fillet_edge, radius=2.0)

    RigidJoint(
        "axle_mount",
        wheel.part,
        Location((z_faces[0].center()), (0, 180, 0)),
    )

show(wheel)
#if "show_object" in locals():
#    show_object(wheel)
    # show_object(wheel.part.joints["axle_mount"].symbol)
