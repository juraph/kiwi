#!/usr/bin/env python3

from build123d import *

lower_hole_vert_loc = 25/2 - 2
lower_hole_hor_loc = 25/2 - 2

with BuildPart() as imu:
   Box(25, 25, 2.0)
   top_face = imu.faces().sort_by()[0]
   with BuildSketch(top_face):
       with Locations((lower_hole_hor_loc, lower_hole_vert_loc)):
           Circle(1.3)
       with Locations((-lower_hole_hor_loc, lower_hole_vert_loc)):
           Circle(1.3)
       with Locations((-lower_hole_hor_loc, -lower_hole_vert_loc)):
           Circle(1.3)
       with Locations((lower_hole_hor_loc, -lower_hole_vert_loc )):
           Circle(1.3)
   extrude(amount=-100, mode=Mode.SUBTRACT)


   RigidJoint("mount", imu.part,
        Location((top_face.center()), (0, 180, 180)),
   )


if 'show_object' in locals():
    show_object(imu)
    show_object(imu.part.joints["mount"].symbol)
