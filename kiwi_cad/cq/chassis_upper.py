#!/usr/bin/env python3

from build123d import *
from build123d import radians
import imu
import battery # Grab the battery dimensions, as the chassis forms a shell around it.

"""
Upper half of the chassis of KIWI
"""

battery_mount_tolerance = 4.0 # Free space around battery in shell

backplate_height, backplate_width, backplate_thickness = (
    163,
    60,
    40,
)
shell_thickness = backplate_thickness/2 - battery.batt_thickness/2 - battery_mount_tolerance

with BuildPart() as u_chassis:

    Box(backplate_width, backplate_height, backplate_thickness)
    topf = u_chassis.faces().sort_by(Axis.Y)[0]
    offset(amount=-shell_thickness, openings=topf)

    # Chamfer(*u_chassis.edges().group_by(Axis.Z)[0] | Axis.Y, length=backplate_thickness - 0.1)
    batt_face = u_chassis.faces(Select.LAST).sort_by(Axis.Y)[-1]
    batt_face_loc = batt_face.center()

    rpi_face = u_chassis.faces().sort_by()[0]
    rpi_cent = rpi_face.center()

    mount_hole_radius = 4
    mount_hole_height = 3

    inner_hole_radius = 2
    inner_hole_depth = 2

    # Place some rails on the rpi face, used for the bolts to mount the rpi
    with BuildSketch(rpi_face):
        with Locations((49/2, 58/2, 0)):
            Circle(mount_hole_radius)
        with Locations((-49/2, 58/2, 0)):
            Circle(mount_hole_radius)
        with Locations((49/2, -58/2, 0)):
            Circle(mount_hole_radius)
        with Locations((-49/2, -58/2, 0)):
            Circle(mount_hole_radius)

    odrive_face = u_chassis.faces().sort_by()[-1]
    # Place mount points for odrive
    with BuildSketch(odrive_face):
        with Locations((50/2 - 3, 153/2, 0)):
            Circle(mount_hole_radius)
        with Locations((-50/2 + 3, 153/2, 0)):
            Circle(mount_hole_radius)
        with Locations((50/2 - 3, -153/2, 0)):
            Circle(mount_hole_radius)
        with Locations((-50/2 + 3, -153/2, 0)):
            Circle(mount_hole_radius)
    extrude(amount=mount_hole_height)

    new_faces_1 = u_chassis.faces().sort_by(Axis.Z)[0:4]
    new_faces_2 = u_chassis.faces().sort_by(Axis.Z)[-4:]

    with BuildSketch(*new_faces_1):
        Circle(radius=inner_hole_radius)
    with BuildSketch(*new_faces_2):
        Circle(radius=inner_hole_radius)
    extrude(amount=-inner_hole_depth, mode=Mode.SUBTRACT)

    # Place a mount point for the lower chassis
    lower_face = u_chassis.faces().sort_by(Axis.Y)[-1]

    RigidJoint("lower_mount", u_chassis.part,
        Location((lower_face.center()), (180, 0, 90)),
    )

    RigidJoint("rpi_mount", u_chassis.part,
        Location((rpi_cent.X, rpi_cent.Y, rpi_cent.Z - 2), (180, 0, 90)),
    )


    RigidJoint("batt_mount", u_chassis.part,
        Location((batt_face_loc), (-90, 0, 0)),
    )

    RigidJoint("odrive_mount", u_chassis.part,
        Location((odrive_face.center()), (0, 180, -90)),
    )

# Render the part
if "show_object" in locals():
    show_object(u_chassis)
    show_object(u_chassis.part.joints["rpi_mount"].symbol)
    show_object(u_chassis.part.joints["odrive_mount"].symbol)
    show_object(u_chassis.part.joints["batt_mount"].symbol)
    show_object(u_chassis.part.joints["lower_mount"].symbol)
