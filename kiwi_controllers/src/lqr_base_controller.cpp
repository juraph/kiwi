#include <algorithm>
#include <cmath>
#include <geometry_msgs/msg/detail/twist__struct.hpp>
#include <rclcpp/rclcpp.hpp>
#include <rclcpp/subscription.hpp>
#include <rclcpp/time.hpp>
#include <sensor_msgs/msg/detail/imu__struct.hpp>
#include <sensor_msgs/msg/detail/joint_state__struct.hpp>
#include <sensor_msgs/msg/imu.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include <std_msgs/msg/detail/float64_multi_array__struct.hpp>
#include <std_msgs/msg/float64_multi_array.hpp>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>
#include <eigen3/unsupported/Eigen/KroneckerProduct>

#include "tf2/LinearMath/Matrix3x3.h"
#include "tf2/LinearMath/Quaternion.h"

#include <geometry_msgs/msg/twist.hpp>

#include <chrono>

#include <memory>

#include <utility>

using namespace std::chrono_literals;
using namespace Eigen;

namespace kiwi_controllers
{
template <typename Derived>
std::string get_shape(const EigenBase<Derived>& x)
{
  std::ostringstream oss;
  oss << "(" << x.rows() << ", " << x.cols() << ")";
  return oss.str();
}

class LQR
{
public:
  LQR()
  {
    // Defining the parameters
    const double g = -9.81;  // gravitational acceleration
    const double m_t = 1.0;  // torso mass
    const double l = 2.1;    // torso length
    const double m_w = 0.2;  // wheel mass
    const double r = 0.6;    // wheel radius
    const double Ib = 1.02;
    const double Iw = 0.1;

    max_solver_iterations_ = 100000;
    desired_solver_error_ = 0.0001;
    dt_ = 0.01;

    A_ << 0, 0, 1.0, -g * l * l * m_t / (r * (Ib * m_t + Ib * m_w + l * l * m_t * m_w)), 0, 0,
        g * l * m_t * (m_t + m_w) / (Ib * m_t + Ib * m_w + l * l * m_t * m_w), 0, 0;

    B_.resize(3, 1);

    B_ << 0.0,
          1.0 * (l*l*m_t + l*m_t*r) / (Iw*l*l*m_t + l*l*m_t*m_w*r*r),
           -1.0 * (Iw + l*m_t*r + m_t*r*r + m_w*r*r) / (Iw*l*l*m_t + l*l*m_t*m_w*r*r);

    // discretise the state space matrices
    Ad = (Id_ - 0.5 * dt_ * Ad).inverse() * (Id_ + 0.5 * dt_ * A_);
    Bd = (Id_ - 0.5 * dt_ * Ad).inverse() * B_ * dt_;

    R_.resize(1, 1);
    R_ << 0.1;

    Q_.diagonal() << 1000, 0, 1;

    K_.resize(1, 3);
    K_.setZero();

    S_.resize(3, 1);
    S_ << 0, 0, 0;
  }

  void solve()
  {
    Eigen::MatrixXd P = Q_;
    Eigen::MatrixXd P_1;
    Eigen::MatrixXd P_err;
    uint iteration_num = 0;
    double curr_err = 10000.0;
    Eigen::MatrixXd::Index maxRow, maxCol;

    while (curr_err > desired_solver_error_ && iteration_num < max_solver_iterations_)
    {
      iteration_num++;
      P_1 = Q_ + Ad.transpose() * P * Ad -
            Ad.transpose() * P * Bd * (R_ + Bd.transpose() * P * Bd).inverse() * Bd.transpose() * P * Ad;
      Eigen::MatrixXd P_err = P_1 - P;
      double err = fabs(P_err.maxCoeff(&maxRow, &maxCol));
      //if (err < curr_err)
        // std::cout << "err :" << err << '\n';
      curr_err = err;
      P = P_1;
    }

    K_ = (R_ + Bd.transpose() * P * Bd).inverse() * Bd.transpose() * P * Ad;
    std::cout << "K " << K_ << '\n';
  }

  MatrixXd K_;

private:
  double desired_solver_error_;
  double dt_;

  uint max_solver_iterations_;

  // Storage for solved Ricatti Solution
  MatrixXd ricatti_solution_;
  MatrixXd lyapunov_solution_;

  // Gain Values
  Matrix3d Q_;
  MatrixXd R_, S_;

  Matrix3d Id_ = Matrix3d::Identity(3, 3);

  Matrix3d A_, Ad;
  MatrixXd B_, Bd;

  Matrix3d A_closed_loop_;
};

class PID
{
public:
  PID() : integral_(0), prev_time_(0.0)
  {
  }

  double gain(const double& target, const double& current_value)
  {
    const double dt = rclcpp::Clock{ RCL_ROS_TIME }.now().seconds() - prev_time_;
    const double error = current_value - target;
    // Reset integral if we have flipped past the zero point
    if (signbit(integral_) != signbit(error))
    {
      integral_ = 0;
    }
    integral_ += error * dt;
    // Clamp our integral
    integral_ = (integral_ < -clamp_) ? -clamp_ : (integral_ > clamp_) ? clamp_ : integral_;
    const double derivative = (error - prev_error_) / dt;
    prev_error_ = error;

    prev_time_ = rclcpp::Clock{ RCL_ROS_TIME }.now().seconds();

    return (p_ * error) + (i_ * integral_) + (d_ * derivative);
  }

  void setGains(const double& p, const double& i, const double& d, const double& i_clamp)
  {
    p_ = p;
    i_ = i;
    d_ = d;
    clamp_ = i_clamp;
    std::cout << "inserting p " << p << std::endl;
    std::cout << "inserting i " << i << std::endl;
    std::cout << "inserting d " << d << std::endl;

    prev_time_ = rclcpp::Clock{ RCL_ROS_TIME }.now().seconds();
  }

  void reset()
  {
    prev_error_ = 0;
    integral_ = 0;
  }

private:
  double p_, i_, d_, clamp_, prev_error_, integral_, prev_time_;
};

class LowPassFilter
{
public:
  LowPassFilter(const double& alpha) : output_(0.0)
  {
    alpha_ = alpha;
  }

  double filter(const double& input)
  {
    output_ = alpha_ * input + (1.0 - alpha_) * output_;
    return output_;
  }

  void reset()
  {
    output_ = 0.0;
  }

private:
  double alpha_;
  double output_;
};

class LqrController : public rclcpp::Node
{
public:
  LqrController()
    : Node("lqr_base_controller")
    , balance_gains_()
    , vel_filter_(0.010)
    , lin_cmd_filter_(0.1)
    , torso_angle_(0.0)
    , lin_cmd_(0.0)
    , ang_cmd_(0.0)
    , wheel_radius_(0.6)
  {
    // Solve the LQR ricatti EQ before continuing.
    balance_gains_.solve();

    balance_pid_.setGains((-balance_gains_.K_(0)), 0.0, 2.0, 0.0);
    linear_pid_.setGains((-balance_gains_.K_(2)), 0.0, 0.01, 0.0);
    angular_pid_.setGains(2, 0.0, 0.01, 0.0);

    cmd_pub_ = this->create_publisher<std_msgs::msg::Float64MultiArray>("/effort_controllers/commands", 10);

    imu_sub_ =
        create_subscription<sensor_msgs::msg::Imu>("/imu/out", 10, [this](const sensor_msgs::msg::Imu::SharedPtr msg) {
          double roll, pitch, yaw;

          tf2::Quaternion tf_quat(msg->orientation.x, msg->orientation.y, msg->orientation.z, msg->orientation.w);
          tf2::Matrix3x3(tf_quat).getRPY(roll, pitch, yaw);
          // Filter the torso angle
          torso_angle_.store(pitch);
        });

    cmd_sub_ = create_subscription<geometry_msgs::msg::Twist>("/cmd_vel", 10,
                                                              [this](const geometry_msgs::msg::Twist::SharedPtr msg) {
                                                                // Convert the twist command to desired wheel velocities
                                                                lin_cmd_.store(lin_cmd_filter_.filter(msg->linear.x / wheel_radius_));
                                                                ang_cmd_.store(msg->angular.z / wheel_radius_);
                                                              });

    vel_sub_ = create_subscription<sensor_msgs::msg::JointState>(
        "/joint_states", 10, [this](const sensor_msgs::msg::JointState::SharedPtr msg) {
          double left_wheel_velocity, right_wheel_velocity;
          for (size_t i = 0; i < msg->name.size(); i++)
          {
            if (msg->name[i] == "left_wheel_joint")
            {
              left_wheel_velocity = msg->velocity[i];
            }
            if (msg->name[i] == "right_wheel_joint")
            {
              right_wheel_velocity = msg->velocity[i];
            }
          }
          curr_lin_vel_.store(vel_filter_.filter((left_wheel_velocity + right_wheel_velocity) / 2));
          curr_ang_vel_.store(right_wheel_velocity - left_wheel_velocity);
        });

    auto ros_clock = rclcpp::Clock::make_shared();
    timer_ = this->create_wall_timer(0.01s, std::bind(&LqrController::timer_callback, this));

    RCLCPP_INFO(this->get_logger(), "Initialised pid base controller");
  }

private:
  void timer_callback()
  {
    // Our torso target is always upright
    auto balance_gain = balance_pid_.gain(torso_angle_, 0.0);

    auto linear_gain = linear_pid_.gain(curr_lin_vel_, lin_cmd_);
    auto angular_gain = angular_pid_.gain(curr_ang_vel_, ang_cmd_);

    auto gain = std::clamp(-balance_gain - linear_gain, -10.0, 10.0);
    auto command = std_msgs::msg::Float64MultiArray();
    command.data.push_back(gain - angular_gain);
    command.data.push_back(gain + angular_gain);
    cmd_pub_->publish(command);
  }

  // Publishes a constant effort
  rclcpp::Publisher<std_msgs::msg::Float64MultiArray>::SharedPtr cmd_pub_;
  rclcpp::Subscription<sensor_msgs::msg::Imu>::SharedPtr imu_sub_;
  rclcpp::Subscription<geometry_msgs::msg::Twist>::SharedPtr cmd_sub_;
  rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr vel_sub_;
  rclcpp::TimerBase::SharedPtr timer_;

  LQR balance_gains_;  // For steady state balance

  PID balance_pid_;
  PID linear_pid_;
  PID angular_pid_;

  LowPassFilter vel_filter_;

  LowPassFilter lin_cmd_filter_;


  geometry_msgs::msg::Twist::SharedPtr cmd_message_;
  std::atomic<double> torso_angle_, lin_cmd_, ang_cmd_, curr_lin_vel_, curr_ang_vel_;

  const double wheel_radius_;
};
}  // namespace kiwi_controllers

int main(int argc, char* argv[])
{
  // Forward command line arguments to ROS
  rclcpp::init(argc, argv);

  // Create a node
  auto node = std::make_shared<kiwi_controllers::LqrController>();

  // Run node until it's exited
  rclcpp::spin(node);

  // Clean up
  rclcpp::shutdown();
  return 0;
}
