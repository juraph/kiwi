#include <cmath>
#include <geometry_msgs/msg/detail/twist__struct.hpp>
#include <rclcpp/rclcpp.hpp>
#include <rclcpp/subscription.hpp>
#include <rclcpp/time.hpp>
#include <sensor_msgs/msg/detail/imu__struct.hpp>
#include <sensor_msgs/msg/detail/joint_state__struct.hpp>
#include <sensor_msgs/msg/imu.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include <std_msgs/msg/detail/float64_multi_array__struct.hpp>
#include <std_msgs/msg/float64_multi_array.hpp>

#include "tf2/LinearMath/Matrix3x3.h"
#include "tf2/LinearMath/Quaternion.h"

#include <geometry_msgs/msg/twist.hpp>

#include <chrono>

#include <memory>
#include <utility>

using namespace std::chrono_literals;
namespace kiwi_controllers {
class PID {
public:
  PID(const double &p, const double &i, const double &d, const double &i_clamp)
      : p_(p), i_(i), d_(d), clamp_(i_clamp), prev_error_(0), integral_(0),
        prev_time_(0.0) {
    prev_time_ = rclcpp::Clock{RCL_ROS_TIME}.now().seconds();
  }

  double gain(const double &target, const double &current_value) {
    const double dt = rclcpp::Clock{RCL_ROS_TIME}.now().seconds() - prev_time_;
    const double error = current_value - target;

    // Reset integral if we have flipped past the zero point
    if (signbit(integral_) != signbit(error)) {
      integral_ = 0;
    }
    integral_ += error * dt;
    // Clamp our integral
    integral_ = (integral_ < -clamp_)  ? -clamp_
                : (integral_ > clamp_) ? clamp_
                                       : integral_;
    const double derivative = (error - prev_error_) / dt;
    prev_error_ = error;

    prev_time_ = rclcpp::Clock{RCL_ROS_TIME}.now().seconds();

    return (p_ * error) + (i_ * integral_) + (d_ * derivative);
  }

  void reset() {
    prev_error_ = 0;
    integral_ = 0;
  }

private:
  double p_, i_, d_, clamp_, prev_error_, integral_, prev_time_;
};

class LowPassFilter {
public:
  LowPassFilter(const double &alpha) : output_(0.0) {
    alpha_ = alpha;
  }

  double filter(const double &input) {
    output_ = alpha_ * input + (1.0 - alpha_) * output_;
    return output_;
  }

  void reset() { output_ = 0.0; }

private:
  double alpha_;
  double output_;
};

class PidController : public rclcpp::Node {
public:
  PidController()
      : Node("pid_base_controller"), balance_gains_(20.0, 0.0, 2.1, 0.0),
        angular_gains_(2.0, 0.0, 0.0, 1.0),
        velocity_gains_(1.1, 0.0, 0.00, 5.0), vel_filter_(0.01),
        torso_angle_(0.0), lin_cmd_(0.0), ang_cmd_(0.0), wheel_radius_(0.6) {

    cmd_pub_ = this->create_publisher<std_msgs::msg::Float64MultiArray>(
        "/effort_controllers/commands", 10);

    imu_sub_ = create_subscription<sensor_msgs::msg::Imu>(
        "/imu/out", 10, [this](const sensor_msgs::msg::Imu::SharedPtr msg) {
          double roll, pitch, yaw;

          tf2::Quaternion tf_quat(msg->orientation.x, msg->orientation.y,
                                  msg->orientation.z, msg->orientation.w);
          tf2::Matrix3x3(tf_quat).getRPY(roll, pitch, yaw);
          // Filter the torso angle
          torso_angle_.store(pitch);
        });

    cmd_sub_ = create_subscription<geometry_msgs::msg::Twist>(
        "/cmd_vel", 10, [this](const geometry_msgs::msg::Twist::SharedPtr msg) {
          // Convert the twist command to desired wheel velocities
          lin_cmd_.store(msg->linear.x / wheel_radius_);
          ang_cmd_.store(msg->angular.z / wheel_radius_);
        });

    vel_sub_ = create_subscription<sensor_msgs::msg::JointState>(
        "/joint_states", 10,
        [this](const sensor_msgs::msg::JointState::SharedPtr msg) {
          double left_wheel_velocity = 0;
          double right_wheel_velocity = 0;
          for (size_t i = 0; i < msg->name.size(); i++) {
            if (msg->name[i] == "left_wheel_joint") {
              left_wheel_velocity = msg->velocity[i];
            }
            if (msg->name[i] == "right_wheel_joint") {
              right_wheel_velocity = msg->velocity[i];
            }
          }
          curr_lin_vel_.store(vel_filter_.filter(
              (left_wheel_velocity + right_wheel_velocity) / 2));
          curr_ang_vel_.store(right_wheel_velocity - left_wheel_velocity);
        });

    auto ros_clock = rclcpp::Clock::make_shared();
    timer_ = this->create_wall_timer(
        0.0005s, std::bind(&PidController::timer_callback, this));

    RCLCPP_INFO(this->get_logger(), "Initialised pid base controller");
  }

private:
  void timer_callback() {
    // Our torso target is always upright
    const double balance_effort = balance_gains_.gain(torso_angle_, 0.0);
    const double linear_effort = velocity_gains_.gain(curr_lin_vel_, lin_cmd_);
    const double angular_effort = angular_gains_.gain(curr_ang_vel_, ang_cmd_);

    /* RCLCPP_INFO(this->get_logger(), "Torso angle: %f", torso_angle_.load()); */
    const double effort = - balance_effort - linear_effort;
    auto command = std_msgs::msg::Float64MultiArray();
    command.data.push_back(effort - angular_effort);
    command.data.push_back(effort + angular_effort);
    cmd_pub_->publish(command);
  }

  // Publishes a constant effort
  rclcpp::Publisher<std_msgs::msg::Float64MultiArray>::SharedPtr cmd_pub_;
  rclcpp::Subscription<sensor_msgs::msg::Imu>::SharedPtr imu_sub_;
  rclcpp::Subscription<geometry_msgs::msg::Twist>::SharedPtr cmd_sub_;
  rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr vel_sub_;
  rclcpp::TimerBase::SharedPtr timer_;

  PID balance_gains_;  // For steady state balance
  PID angular_gains_;  // For Changing directions, or stopping
  PID velocity_gains_; // For achieving a desired velocity

  LowPassFilter vel_filter_;

  geometry_msgs::msg::Twist::SharedPtr cmd_message_;
  std::atomic<double> torso_angle_, lin_cmd_, ang_cmd_, curr_lin_vel_,
      curr_ang_vel_;

  const double wheel_radius_;
};
} // namespace kiwi_controllers

int main(int argc, char *argv[]) {

  // Forward command line arguments to ROS
  rclcpp::init(argc, argv);

  // Create a node
  auto node = std::make_shared<kiwi_controllers::PidController>();

  // Run node until it's exited
  rclcpp::spin(node);

  // Clean up
  rclcpp::shutdown();
  return 0;
}
