#include <Eigen/Core>
#include <Eigen/Dense>
#include <algorithm>
#include <casadi/core/dm_fwd.hpp>
#include <casadi/core/function.hpp>
#include <casadi/core/slice.hpp>
#include <casadi/core/sparsity_interface.hpp>
#include <casadi/core/sx_fwd.hpp>
#include <cmath>
#include <geometry_msgs/msg/detail/twist__struct.hpp>
#include <random>
#include <rclcpp/rclcpp.hpp>
#include <rclcpp/subscription.hpp>
#include <rclcpp/time.hpp>
#include <sensor_msgs/msg/detail/imu__struct.hpp>
#include <sensor_msgs/msg/detail/joint_state__struct.hpp>
#include <sensor_msgs/msg/imu.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include <std_msgs/msg/detail/float64_multi_array__struct.hpp>
#include <std_msgs/msg/float64_multi_array.hpp>

#include "tf2/LinearMath/Matrix3x3.h"
#include "tf2/LinearMath/Quaternion.h"

#include <geometry_msgs/msg/twist.hpp>

#include "kiwi_controllers/low_pass_filter.hpp"
#include "kiwi_controllers/pid.hpp"

#include <chrono>
#include <memory>
#include <utility>

#include <casadi/casadi.hpp>

using namespace std::chrono_literals;
using namespace Eigen;
using namespace casadi;

namespace kiwi_controllers {

std::random_device rd;
std::mt19937 gen(rd());
std::uniform_real_distribution<float> rand_dist(-0.4f, 0.4f);

template <typename Derived> std::string get_shape(const EigenBase<Derived> &x) {
  std::ostringstream oss;
  oss << "(" << x.rows() << ", " << x.cols() << ")";
  return oss.str();
}

class MPC {
public:
  MPC() {


    casadi::SX th_0 = 0; // Initial angle
    casadi::SX dot_th_0 = 0; // Initial angular velocity
    casadi::SX v_0 = 0;      // Initial wheel velocity

  }

  void step(const double &theta, const double &targ) {
    (void)targ;
    /* std::cout << "iter with theta: " << theta << std::endl; */

    casadi::SX u = casadi::SX::sym("u", nu_);

    // Trajectory
    std::vector<casadi::SX> th_k, v_k, dot_th_k;

    // FIXME Replace with actual joint readings
    SX th = theta, v = 0, dot_th = 0;

    // Lambda function for calculating the state space update
    auto ss_delta = [](const auto &theta, auto theta_ddot) {
      (void)theta_ddot;

      const auto id1_numerator = 0; // b_b * theta_dot Ignore damping for now
      const auto id2_numerator = 0; // b_w * omega_dot Ignore damping for now
      const auto id3_numerator = -Ib - (l * l) * m_t;
      const auto id4_numerator = g * l * m_t * casadi::SX::sin(theta);

      const auto state_denominator =
          -Ib - (l * l) * m_t + l * m_t * r * casadi::SX::cos(theta);

      /* const double input_numerator_1 = */
      /*     g * l * m_t * (r * r) * (m_t + m_w) * std::sin(theta); */
      /* const double input_numerator_2 = */
      /*     -l * l * m_t * m_t * r * r * std::cos(theta) * std::cos(theta); */
      /* const double input_numerator_3 = */
      /*     (l * l * m_t * m_t * r * r * std::cos(theta) * std::cos(theta) - */
      /*      l * l * m_t * r * r * (m_t + m_w)) * theta_ddot; */

      casadi::SX omega =
          (id1_numerator + id2_numerator + id3_numerator + id4_numerator) /
          state_denominator;
      return omega;
    };

    // Integrate over interval with euler forward TODO (Juraph) Replace with RK4
    for (uint k = 0; k < nu_; ++k) {
      for (uint j = 0; j < nj_; ++j) {
        th += dt * dot_th; // Integrate theta vel to get theta
        dot_th += u(k); /* ? TODO (Juraph) I think this follows the trajectory?
                     Not entirely sure how this should behave */
        v += dt * (ss_delta(th, dot_th));
      }
      th_k.push_back(th);
      dot_th_k.push_back(dot_th);
      v_k.push_back(v); // Wheel velocity vector
    }

    casadi::SX th_all = casadi::SX::vertcat(th_k);
    casadi::SX dot_th_all = casadi::SX::vertcat(dot_th_k);
    casadi::SX v_all = casadi::SX::vertcat(v_k);

    // Objective function
    casadi::SX f = casadi::SX::dot(th, th) + casadi::SX::dot(u, u);

    // Terminal Constraints
    /* casadi::SX g = casadi::SX::vertcat({th_all, dot_th_all, v, v_all}); */
    casadi::SX g = casadi::SX::vertcat({th, v, v_all});

    // Create the NLP
    casadi::SXDict nlp = {{"x", u}, {"f", f}, {"g", g}};

    // Attempt to limit wall time, allowing for (MEBE) real-time goodness
    /* casadi::Dict opts = {{"max_wall_time", 1.0}, {"ipopt.print_level", 0}, {"print_time", 0}}; */
    casadi::Dict opts = {{"ipopt.print_level", 0}, {"print_time", 0}};

    // Allocate an NLP solver and buffers
    Function solver = casadi::nlpsol("solver", "ipopt", nlp, opts);

    // Bounds on g
    std::vector<double> gmin = {10, 0};
    std::vector<double> gmax = {10, 0};
    gmin.resize(2 + nu_, -std::numeric_limits<double>::infinity());
    gmax.resize(2 + nu_, 2.14);

    // Solve the problem
    DMDict arg = {{"lbx", -10.5},
                  {"ubx", 10.5},
                  {"x0", 0.0},
                  {"lbg", gmin},
                  {"ubg", gmax}};
    DMDict res = solver(arg);

    // Print the optimal cost
    /* double cost(res.at("f")); */
    /* std::cout << "optimal cost: " << cost << std::endl; */

    // Print the optimal solution
    std::vector<double> uopt(res.at("x"));
    /* std::cout << "optimal control: " << uopt << std::endl; */

    // Get the state trajectory
    Function xfcn("xfcn", {u}, {th_all, v_all, dot_th_all});
    std::vector<double> thopt, vopt, dot_th_opt;
    xfcn({uopt}, {&thopt, &vopt, &dot_th_opt});
    /* std::cout << "theta" << thopt << std::endl; */
    /* std::cout << "omega" << vopt << std::endl; */
    /* std::cout << "theta dot:     " << dot_th_opt << std::endl; */
  }

  void initialise() {}

  MatrixXd K_;

private:
  const uint nu_ = 10; // number of control segments
  const uint nj_ = 30; // number of integration steps per segment

  casadi::SX dt = 10.0 / (nj_ * nu_);

  static constexpr float g = -9.81; // gravitational acceleration
  static constexpr float m_t = 1.0; // torso mass
  static constexpr float l = 2.1;   // torso length
  static constexpr float m_w = 0.2; // wheel mass
  static constexpr float r = 0.6;   // wheel radius
  static constexpr float Ib = 1.02;
  static constexpr float Iw = 0.1;

};

class MPCController : public rclcpp::Node {
public:
  MPCController()
      : Node("lqr_base_controller"), balance_gains_(), vel_filter_(0.010),
        lin_cmd_filter_(0.1), torso_angle_(0.0), lin_cmd_(0.0), ang_cmd_(0.0),
        wheel_radius_(0.6) {

    // Initialise the MPC
    /* balance_gains_.initialise(); */

    /* balance_pid_.setGains((-balance_gains_.K_(0)), 0.0, 2.0, 0.0); */
    /* linear_pid_.setGains((-balance_gains_.K_(2)), 0.0, 0.01, 0.0); */
    angular_pid_.setGains(2, 0.0, 0.01, 0.0);

    cmd_pub_ = this->create_publisher<std_msgs::msg::Float64MultiArray>(
        "/effort_controllers/commands", 10);

    imu_sub_ = create_subscription<sensor_msgs::msg::Imu>(
        "/imu/out", 10, [this](const sensor_msgs::msg::Imu::SharedPtr msg) {
          double roll, pitch, yaw;

          tf2::Quaternion tf_quat(msg->orientation.x, msg->orientation.y,
                                  msg->orientation.z, msg->orientation.w);
          tf2::Matrix3x3(tf_quat).getRPY(roll, pitch, yaw);
          // Filter the torso angle
          torso_angle_.store(pitch);
        });

    cmd_sub_ = create_subscription<geometry_msgs::msg::Twist>(
        "/cmd_vel", 10, [this](const geometry_msgs::msg::Twist::SharedPtr msg) {
          // Convert the twist command to desired wheel velocities
          lin_cmd_.store(lin_cmd_filter_.filter(msg->linear.x / wheel_radius_));
          ang_cmd_.store(msg->angular.z / wheel_radius_);
        });

    vel_sub_ = create_subscription<sensor_msgs::msg::JointState>(
        "/joint_states", 10,
        [this](const sensor_msgs::msg::JointState::SharedPtr msg) {
          double left_wheel_velocity = 0;
          double right_wheel_velocity = 0;
          for (size_t i = 0; i < msg->name.size(); i++) {
            if (msg->name[i] == "left_wheel_joint") {
              left_wheel_velocity = msg->velocity[i];
            }
            if (msg->name[i] == "right_wheel_joint") {
              right_wheel_velocity = msg->velocity[i];
            }
          }
          curr_lin_vel_.store(vel_filter_.filter(
              (left_wheel_velocity + right_wheel_velocity) / 2));
          curr_ang_vel_.store(right_wheel_velocity - left_wheel_velocity);
        });

    auto ros_clock = rclcpp::Clock::make_shared();
    timer_ = this->create_wall_timer(
        1.01s, std::bind(&MPCController::timer_callback, this));

    RCLCPP_INFO(this->get_logger(), "Initialised MPC base controller");
  }

private:
  void timer_callback() {
    // Our torso target is always upright
    /* auto balance_gain = balance_pid_.gain(torso_angle_, 0.0); */

    /* auto linear_gain = linear_pid_.gain(curr_lin_vel_, lin_cmd_); */
    /* auto angular_gain = angular_pid_.gain(curr_ang_vel_, ang_cmd_); */
    /* auto rand_ang = rand_dist(rd); */

    balance_gains_.step(torso_angle_, lin_cmd_);
    /* auto gain = std::clamp(-balance_gain - linear_gain, -10.0, 10.0); */
    auto command = std_msgs::msg::Float64MultiArray();
    command.data.push_back(0.0);
    command.data.push_back(0.0);
    cmd_pub_->publish(command);
  }

  // Publishes a constant effort
  rclcpp::Publisher<std_msgs::msg::Float64MultiArray>::SharedPtr cmd_pub_;
  rclcpp::Subscription<sensor_msgs::msg::Imu>::SharedPtr imu_sub_;
  rclcpp::Subscription<geometry_msgs::msg::Twist>::SharedPtr cmd_sub_;
  rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr vel_sub_;
  rclcpp::TimerBase::SharedPtr timer_;

  MPC balance_gains_; // For steady state balance

  PID balance_pid_;
  PID linear_pid_;
  PID angular_pid_;

  LowPassFilter vel_filter_;

  LowPassFilter lin_cmd_filter_;

  geometry_msgs::msg::Twist::SharedPtr cmd_message_;
  std::atomic<double> torso_angle_, lin_cmd_, ang_cmd_, curr_lin_vel_,
      curr_ang_vel_;

  const double wheel_radius_;
};
} // namespace kiwi_controllers

int main(int argc, char *argv[]) {
  // Forward command line arguments to ROS
  rclcpp::init(argc, argv);

  // Create a node
  auto node = std::make_shared<kiwi_controllers::MPCController>();

  // Run node until it's exited
  rclcpp::spin(node);

  // Clean up
  rclcpp::shutdown();
  return 0;
}
