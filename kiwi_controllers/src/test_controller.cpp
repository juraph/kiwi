#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/detail/float64_multi_array__struct.hpp>
#include <std_msgs/msg/float64_multi_array.hpp>

#include <chrono>

#include <memory>
#include <utility>

using namespace std::chrono_literals;

class TestEffort : public rclcpp::Node {
public:
  TestEffort() : Node("test_effort") {

    auto default_qos = rclcpp::QoS(rclcpp::SystemDefaultsQoS());
    cmd_pub_ = this->create_publisher<std_msgs::msg::Float64MultiArray>(
        "/effort_controllers/commands", default_qos);

    auto ros_clock = rclcpp::Clock::make_shared();
    timer_ = this->create_wall_timer(
        2s, std::bind(&TestEffort::timer_callback, this));

    RCLCPP_INFO(this->get_logger(), "Initialised effort test controller");
  }

private:

  void timer_callback() {
    auto command = std_msgs::msg::Float64MultiArray();
    command.data.push_back(3.0);
    command.data.push_back(3.0);
    cmd_pub_->publish(command);
  }

  // Publishes a constant effort
  rclcpp::Publisher<std_msgs::msg::Float64MultiArray>::SharedPtr cmd_pub_;
  rclcpp::TimerBase::SharedPtr timer_;
};

int main(int argc, char *argv[]) {
  // Forward command line arguments to ROS
  rclcpp::init(argc, argv);

  // Create a node
  auto node = std::make_shared<TestEffort>();

  // Run node until it's exited
  rclcpp::spin(node);

  // Clean up
  rclcpp::shutdown();
  return 0;
}
