#include <fcntl.h>
#include <geometry_msgs/msg/twist.hpp>
#include <mutex>
#include <queue>
#include <rclcpp/rclcpp.hpp>
#include <termios.h>
#include <thread>
#include <unistd.h>

using namespace std::chrono_literals;

class KeyboardControlNode : public rclcpp::Node {
public:
  KeyboardControlNode() : Node("keyboard_control_node"), no_key_pressed_(true) {
    // Create a publisher for the command velocities
    velocity_publisher_ =
        this->create_publisher<geometry_msgs::msg::Twist>("cmd_vel", 10);

    // Initialize the twist message
    twist_msg_.linear.x = 0.0;
    twist_msg_.angular.z = 0.0;

    // Set up keyboard input
    setTerminalInput(true);

    // Create a thread for reading keyboard input
    read_thread_ = std::thread(&KeyboardControlNode::readKeyboardInput, this);

    timer_ = this->create_wall_timer(
        0.1s, std::bind(&KeyboardControlNode::publishTwist, this));
  }

  ~KeyboardControlNode() {
    // Restore the terminal settings
    setTerminalInput(false);

    // Join the read thread
    if (read_thread_.joinable()) {
      read_thread_.join();
    }
  }

private:
  void publishTwist() {
    geometry_msgs::msg::Twist msg;

    // Lock the mutex to access the twist message
    std::lock_guard<std::mutex> lock(mutex_);
    msg = twist_msg_;
    std::cout << "x: " << msg.linear.x << std::endl;
    std::cout << "z: " << msg.angular.z << std::endl;

    // Publish the twist message
    velocity_publisher_->publish(msg);
    twist_msg_.angular.z = 0;
    twist_msg_.linear.x = 0;
    no_key_pressed_ = true;
  }

  void setTerminalInput(bool enable) {
    struct termios t;
    if (tcgetattr(STDIN_FILENO, &t) < 0) {
      RCLCPP_ERROR(this->get_logger(), "Failed to get terminal attributes");
      return;
    }

    if (enable) {
      t.c_lflag &= ~(ICANON | ECHO);
    } else {
      t.c_lflag |= (ICANON | ECHO);
    }

    if (tcsetattr(STDIN_FILENO, TCSANOW, &t) < 0) {
      RCLCPP_ERROR(this->get_logger(), "Failed to set terminal attributes");
    }
  }

  void readKeyboardInput() {
    char c;
    while (rclcpp::ok()) {
      // Lock the mutex before modifying the twist message

      // Read a character from the keyboard
      if (read(STDIN_FILENO, &c, 1) < 0) {
        RCLCPP_ERROR(this->get_logger(), "Failed to read from keyboard");
        continue;
      }

      std::lock_guard<std::mutex> lock(mutex_);

      no_key_pressed_ = false;

      // Update twist message based on keyboard input
      switch (c) {
      case 'w':
        twist_msg_.linear.x = 2.5; // Forward motion
        twist_msg_.linear.z = 0.0;
        break;
      case 's':
        twist_msg_.linear.x = -2.5; // Backward motion
        twist_msg_.linear.z = 0.0;
        break;
      case 'a':
        twist_msg_.linear.x = 0.0;
        twist_msg_.angular.z = 1.5; // Rotate left
        break;
      case 'd':
        twist_msg_.linear.x = 0.0;
        twist_msg_.angular.z = -1.5; // Rotate right
        break;
      default:
        twist_msg_.linear.x = 0.0;
        twist_msg_.angular.z = 0.0;
        break;
      }
      // rclcpp::sleep_for(std::chrono::seconds(0.01s));
    }
  }

  rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr velocity_publisher_;
  rclcpp::TimerBase::SharedPtr timer_;
  geometry_msgs::msg::Twist twist_msg_;
  std::thread read_thread_;
  std::mutex mutex_;
  bool no_key_pressed_;
};

int main(int argc, char **argv) {
  rclcpp::init(argc, argv);
  auto node = std::make_shared<KeyboardControlNode>();

  rclcpp::spin(node);

  rclcpp::shutdown();
  return 0;
}
