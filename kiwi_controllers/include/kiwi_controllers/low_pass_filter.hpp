#ifndef LOW_PASS_FILTER_H_
#define LOW_PASS_FILTER_H_


namespace kiwi_controllers
{
class LowPassFilter
{
public:
  LowPassFilter(const double& alpha) : output_(0.0)
  {
    alpha_ = alpha;
  }

  double filter(const double& input)
  {
    output_ = alpha_ * input + (1.0 - alpha_) * output_;
    return output_;
  }

  void reset()
  {
    output_ = 0.0;
  }

private:
  double alpha_;
  double output_;
};

}

#endif // LOW_PASS_FILTER_H_
