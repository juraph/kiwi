#ifndef  KIWI_PID_
#define KIWI_PID_


#include <rclcpp/rclcpp.hpp>

namespace kiwi_controllers
{

class PID
{
public:
  PID() : integral_(0), prev_time_(0.0)
  {
  }

  double gain(const double& target, const double& current_value)
  {
    const double dt = rclcpp::Clock{ RCL_ROS_TIME }.now().seconds() - prev_time_;
    const double error = current_value - target;
    // Reset integral if we have flipped past the zero point
    if (std::signbit(integral_) != std::signbit(error))
    {
      integral_ = 0;
    }
    integral_ += error * dt;
    // Clamp our integral
    integral_ = (integral_ < -clamp_) ? -clamp_ : (integral_ > clamp_) ? clamp_ : integral_;
    const double derivative = (error - prev_error_) / dt;
    prev_error_ = error;

    prev_time_ = rclcpp::Clock{ RCL_ROS_TIME }.now().seconds();

    return (p_ * error) + (i_ * integral_) + (d_ * derivative);
  }

  void setGains(const double& p, const double& i, const double& d, const double& i_clamp)
  {
    p_ = p;
    i_ = i;
    d_ = d;
    clamp_ = i_clamp;
    std::cout << "inserting p " << p << std::endl;
    std::cout << "inserting i " << i << std::endl;
    std::cout << "inserting d " << d << std::endl;

    prev_time_ = rclcpp::Clock{ RCL_ROS_TIME }.now().seconds();
  }

  void reset()
  {
    prev_error_ = 0;
    integral_ = 0;
  }

private:
  double p_, i_, d_, clamp_, prev_error_, integral_, prev_time_;
};
}

#endif
