#!/usr/bin/env bash
# Check architecture to determine if we're on the real robot
arch=$(uname -m)
if [[  $arch == "aarch64" ]]; then
    echo "RPI Detected, building hw docker"
    docker build -t kiwi_docker -f hw_dockerfile .
else
    echo "Development PC detected, building with gazebo"
    docker build -t kiwi_docker .
fi
