#!/usr/bin/env bash
docker container stop kiwi_docker
docker container rm kiwi_docker
docker run --network=host --ipc=host --pid=host --privileged --ulimit nofile=1024:2056 --env="DISPLAY" -it -v /dev/shm:/dev/shm -v ~/ros2_ws/kiwi_ws:/home/kiwi/kiwi_ws:Z --name kiwi_docker kiwi_docker 
