#ifndef KIWI_DRAKE_IMU_ROS_INTERFACE_HPP_
#define KIWI_DRAKE_IMU_ROS_INTERFACE_HPP_

#include <drake/systems/framework/diagram_builder.h>
#include <drake/systems/framework/leaf_system.h>


namespace kiwi_drake
{

    template <typename T>
    class IMUROSInterface drake::systems::LeafSystem<T>
    {
    public:
      DRAKE_NO_COPY_NO_MOVE_NO_ASSIGN(IMUROSInterface)

}
#endif // KIWI_DRAKE_IMU_ROS_INTERFACE_HPP_
