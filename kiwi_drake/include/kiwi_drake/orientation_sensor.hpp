#ifndef KIWI_DRAKE_IMU_ROS_INTERFACE_HPP_
#define KIWI_DRAKE_IMU_ROS_INTERFACE_HPP_

#include <vector>

#include "tf2/LinearMath/Quaternion.h"

#include <drake/common/drake_copyable.h>
#include <drake/math/rotation_matrix.h>
#include <drake/multibody/plant/multibody_plant.h>
#include <drake/multibody/tree/multibody_tree_indexes.h>
#include <drake/systems/framework/diagram_builder.h>
#include <drake/systems/framework/leaf_system.h>

namespace kiwi_drake
{
    template <typename T>
    class OrientationSensor final : public drake::systems::LeafSystem<T>
    {
    public:
      DRAKE_NO_COPY_NO_MOVE_NO_ASSIGN(OrientationSensor)

      OrientationSensor(const drake::multibody::Body<T> &body,
                       const drake::math::RigidTransform<double> &X_BS);

      template <typename U>
      explicit OrientationSensor(const OrientationSensor<U> &);

      const drake::systems::InputPort<T> &get_body_poses_input_port() const
      {
        return *body_poses_input_port_;
      }

      const drake::multibody::BodyIndex &body_index() const { return body_index_; }

      const drake::math::RigidTransform<double> &pose() const
      {
        return X_BS_;
      }

      static const OrientationSensor &AddToDiagram(
          const drake::multibody::Body<T> &body, const drake::math::RigidTransform<double> &X_BS,
          const drake::multibody::MultibodyPlant<T> &plant, drake::systems::DiagramBuilder<T> *builder);

    private:
      OrientationSensor(const drake::multibody::BodyIndex &body_index,
                       const drake::math::RigidTransform<double> &X_BS);

      void CalcOutput(const drake::systems::Context<T> &context, drake::systems::BasicVector<T> *output) const;

      const drake::multibody::BodyIndex body_index_;
      const drake::math::RigidTransform<double> X_BS_;
      const drake::systems::InputPort<T> *body_poses_input_port_{nullptr};
      const drake::systems::OutputPort<T> *measurement_output_port_{nullptr};
    };

}

#endif
