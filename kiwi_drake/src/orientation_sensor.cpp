#include "kiwi_drake/orientation_sensor.hpp"

#include <drake/multibody/math/spatial_algebra.h>

namespace kiwi_drake
    {
        using drake::math::RigidTransform;
        using drake::math::RotationMatrix;
        using drake::systems::BasicVector;
        using drake::systems::Context;
        using drake::systems::DiagramBuilder;
        using drake::systems::LeafSystem;
        using drake::systems::SystemTypeTag;

        template <typename T>
        OrientationSensor<T>::OrientationSensor(const drake::multibody::Body<T> &body,
                                              const RigidTransform<double> &X_BS)
            : OrientationSensor(body.index(), X_BS) {}

        template <typename T>
        OrientationSensor<T>::OrientationSensor(
            const drake::multibody::BodyIndex &body_index,
            const RigidTransform<double> &X_BS)
            : LeafSystem<T>(SystemTypeTag<OrientationSensor>{}),
              body_index_(body_index),
              X_BS_(X_BS)
        {
            measurement_output_port_ = &this->DeclareVectorOutputPort(
                "measurement", 4, &OrientationSensor<T>::CalcOutput);

            body_poses_input_port_ = &this->DeclareAbstractInputPort(
                "body_poses", drake::Value<std::vector<RigidTransform<T>>>());
        }

        template <typename T>
        void OrientationSensor<T>::CalcOutput(const Context<T> &context,
                                             BasicVector<T> *output) const
        {
            const auto &X_WB =
                get_body_poses_input_port().template Eval<std::vector<RigidTransform<T>>>(
                    context)[body_index_];

            const auto R_SB = X_BS_.rotation().matrix().template cast<T>().transpose();
            const auto R_BW = X_WB.rotation().matrix().transpose();
            const auto R_SW = R_SB * R_BW;

            output->SetFromVector(
                RotationMatrix<T>(R_SW.transpose()).ToQuaternionAsVector4());
        }

        template <typename T>
        const OrientationSensor<T> &OrientationSensor<T>::AddToDiagram(
            const drake::multibody::Body<T> &body, const RigidTransform<double> &X_BS,
            const drake::multibody::MultibodyPlant<T> &plant,
            DiagramBuilder<T> *builder)
        {
            const auto &orientation_sensor =
                *builder->template AddSystem<OrientationSensor<T>>(body, X_BS);
            builder->Connect(plant.get_body_poses_output_port(),
                             orientation_sensor.get_body_poses_input_port());
            return orientation_sensor;
        }

        template <typename T>
        template <typename U>
        OrientationSensor<T>::OrientationSensor(const OrientationSensor<U> &other)
            : OrientationSensor(other.body_index(), other.pose()) {}

        DRAKE_DEFINE_CLASS_TEMPLATE_INSTANTIATIONS_ON_DEFAULT_SCALARS(
            class kiwi_drake::OrientationSensor)

    }
