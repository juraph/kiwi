#include <array>
#include <drake/systems/framework/basic_vector.h>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <limits>
#include <memory>

#include <rclcpp/rclcpp.hpp>

#include <sensor_msgs/msg/imu.hpp>
#include <sensor_msgs/msg/joint_state.hpp>

#include "drake/systems/framework/diagram_builder.h"
#include <drake/common/find_resource.h>
#include <drake/geometry/drake_visualizer.h>
#include <drake/geometry/meshcat_visualizer.h>
#include <drake/multibody/parsing/parser.h>
#include <drake/multibody/plant/multibody_plant.h>
#include <drake/multibody/plant/multibody_plant_config_functions.h>
#include <drake/systems/analysis/simulator.h>
#include <drake/systems/framework/diagram_builder.h>
#include <drake/systems/primitives/constant_vector_source.h>
#include <drake/systems/primitives/vector_log_sink.h>
#include <drake/systems/sensors/accelerometer.h>
#include <drake/systems/sensors/gyroscope.h>
#include <drake_ros/core/clock_system.h>
#include <drake_ros/core/drake_ros.h>
#include <drake_ros/core/ros_interface_system.h>
#include <drake_ros/core/ros_publisher_system.h>
#include <drake_ros/tf2/scene_tf_broadcaster_system.h>
#include <drake_ros/viz/rviz_visualizer.h>
#include <gflags/gflags.h>
#include <rclcpp/logging.hpp>

#include <std_msgs/msg/float64_multi_array.hpp>

#include "kiwi_drake/imu.hpp"

DEFINE_double(simulation_sec, std::numeric_limits<double>::infinity(),
              "How many seconds to run the simulation");

using drake_ros::core::ClockSystem;
using drake_ros::core::DrakeRos;
using drake_ros::core::RosInterfaceSystem;
using drake_ros::core::RosPublisherSystem;

using drake::systems::ConstantVectorSource;
using drake::systems::Simulator;
using drake::systems::TriggerType;

std::string exec(const char *cmd) {
  std::array<char, 128> buffer;
  std::string result;
  std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
  if (!pipe) {
    throw std::runtime_error("popen() failed!");
  }
  while (fgets(buffer.data(), static_cast<int>(buffer.size()), pipe.get()) !=
         nullptr) {
    result += buffer.data();
  }
  return result;
}

class ArrayInputSystem : public drake::systems::LeafSystem<double> {
public:
  ArrayInputSystem(const std::array<double, 2> &cmd_vel) : cmd_vel_(cmd_vel) {
    this->DeclareVectorOutputPort("torque",
                                  drake::systems::BasicVector<double>(2),
                                  &ArrayInputSystem::CalcOutput);
  }

private:
  void CalcOutput(const drake::systems::Context<double> & /*context*/,
                  drake::systems::BasicVector<double> *output) const {
    output->set_value(Eigen::Vector2d{std::clamp(cmd_vel_[0], -30.0, 30.0),
                                      std::clamp(cmd_vel_[1], -30.0, 30.0)});
  }

  const std::array<double, 2> &cmd_vel_;
};

template <typename T>
class RosQuaternionConverter : public drake::systems::LeafSystem<T> {
public:
  RosQuaternionConverter() {
    this->DeclareVectorInputPort("drake_quat",
                                 drake::systems::BasicVector<T>(4));

    this->DeclareAbstractOutputPort("ros_quat",
                                    &RosQuaternionConverter::CalcOutput);
  }

private:
  void CalcOutput(const drake::systems::Context<double> &context,
                  sensor_msgs::msg::Imu *output) const {
    const auto current_rot = this->get_input_port(input_port_).Eval(context);
    output->orientation.w = current_rot[0];
    output->orientation.x = current_rot[1];
    output->orientation.y = current_rot[2];
    output->orientation.z = current_rot[3];
  }
  int input_port_ = 0;
};

template <typename T>
class JointStatePublisherConverter : public drake::systems::LeafSystem<T> {
public:
  JointStatePublisherConverter(const drake::multibody::MultibodyPlant<T>& plant,
                               const std::vector<std::string>& joint_names)
      : plant_(plant), joint_names_(joint_names) {
    this->DeclareVectorInputPort("plant_state",
                                 plant.num_multibody_states());
    this->DeclareAbstractOutputPort("joint_states",
                                    &JointStatePublisherConverter::CalcOutput);

    auto num_positions = plant.GetPositionNames().size();
    // Store the position and velocity start indices for each joint
    for (const auto& name : joint_names) {
      const auto& joint = plant_.GetJointByName(name);
      joint_indices_.push_back({joint.position_start(), joint.velocity_start()+num_positions});
    }
  }

private:
  void CalcOutput(const drake::systems::Context<T>& context,
                  sensor_msgs::msg::JointState* output) const {
    const Eigen::VectorXd current_state = this->get_input_port().Eval(context);

    output->name = joint_names_;
    output->position.resize(joint_names_.size());
    output->velocity.resize(joint_names_.size());
    output->effort.resize(joint_names_.size(), 0.0);

    for (size_t i = 0; i < joint_names_.size(); ++i) {
      output->position[i] = current_state[joint_indices_[i].first];
      output->velocity[i] = current_state[joint_indices_[i].second];
    }
  }

  const drake::multibody::MultibodyPlant<T>& plant_;
  std::vector<std::string> joint_names_;
  std::vector<std::pair<int, int>> joint_indices_;  // Pairs of (position_index, velocity_index)
};


// CMD vel for joint commands
std::array<double, 2> cmd_vel_;

int main(int /*argc*/, char ** /*argv*/) {
  /* gflags::ParseCommandLineFlags(&argc, &argv, true); */
  // Create a Drake diagram
  drake::systems::DiagramBuilder<double> builder;

  // Initilise the ROS infrastructure
  drake_ros::core::init();
  // Create a Drake system to interface with ROS
  auto ros_interface_system = builder.AddSystem<RosInterfaceSystem>(
      std::make_unique<DrakeRos>("kiwi_drake_node"));

  auto direct_ros_node = rclcpp::Node::make_shared("interface");
  auto logger =
      ros_interface_system->get_ros_interface()->get_node().get_logger();

  auto cmd_vel_sub =
      direct_ros_node->create_subscription<std_msgs::msg::Float64MultiArray>(
          "/effort_controllers/commands", 10,
          [&](const std_msgs::msg::Float64MultiArray::SharedPtr msg) {
            auto t_arr = std::array{msg->data[0], msg->data[1]};
            std::swap(t_arr, cmd_vel_);
          });
  ClockSystem::AddToBuilder(&builder,
                            ros_interface_system->get_ros_interface());

  // Add a multibody plant and a scene graph to hold the robots
  drake::multibody::MultibodyPlantConfig plant_config;
  plant_config.time_step = 0.001;
  plant_config.discrete_contact_approximation = "sap";
  auto [plant, scene_graph] =
      drake::multibody::AddMultibodyPlant(plant_config, &builder);

  const double viz_dt = 1 / 32.0;
  // Add a TF2 broadcaster to provide task frame information
  auto scene_tf_broadcaster =
      builder.AddSystem<drake_ros::tf2::SceneTfBroadcasterSystem>(
          ros_interface_system->get_ros_interface(),
          drake_ros::tf2::SceneTfBroadcasterParams{
              {TriggerType::kPeriodic}, viz_dt, "/tf"});
  builder.Connect(scene_graph.get_query_output_port(),
                  scene_tf_broadcaster->get_graph_query_input_port());

  // Add model of the ground.
  const double static_friction = 1.0;
  const Eigen::Vector4<double> green(0.5, 1.0, 0.5, 1.0);
  plant.RegisterVisualGeometry(
      plant.world_body(), drake::math::RigidTransformd(),
      drake::geometry::HalfSpace(), "GroundVisualGeometry", green);
  // For a time-stepping model only static friction is used.
  const drake::multibody::CoulombFriction<double> ground_friction(
      static_friction, static_friction);

  plant.RegisterCollisionGeometry(
      plant.world_body(), drake::math::RigidTransformd(),
      drake::geometry::HalfSpace(), "GroundCollisionGeometry", ground_friction);

  // Add a system to output the visualisation markers for rviz
  auto scene_visualizer = builder.AddSystem<drake_ros::viz::RvizVisualizer>(
      ros_interface_system->get_ros_interface(),
      drake_ros::viz::RvizVisualizerParams{
          {TriggerType::kPeriodic}, viz_dt, true});
  builder.Connect(scene_graph.get_query_output_port(),
                  scene_visualizer->get_graph_query_input_port());

  // Prepare to load the robot model
  auto model_file =
      exec("xacro /home/kiwi/kiwi_ws/src/kiwi_gazebo/urdf/kiwi.xacro.urdf");
  const std::string model_name = "kiwi";

  // Create an array of kiwis
  drake::multibody::ModelInstanceIndex model_index;

  // Load the model from the file and give it a name based on its X and Y
  // coordinates in the array
  auto parser = drake::multibody::Parser(&plant);
  parser.SetAutoRenaming(true);
  auto model_instance = parser.AddModelsFromString(model_file, "urdf")[0];

  auto X_WB = drake::math::RigidTransform(drake::Vector3<double>{
      static_cast<double>(0.), static_cast<double>(0.), 1.3});

  plant.SetDefaultFreeBodyPose(plant.GetBodyByName("base_link", model_instance),
                               X_WB);

  Eigen::Vector3d imu_in_pelvis(0.03155, 0, -0.07996);
  const drake::math::RigidTransform<double> X_BS(imu_in_pelvis);
  const auto &body = plant.GetBodyByName("base_link");

  // Finalise the multibody plant to make it ready for use
  plant.Finalize();

  auto imu = builder.AddSystem<kiwi_drake::IMU>(
      body, plant.GetFrameByName("imu_link").GetFixedPoseInBodyFrame(),
      plant.gravity_field().gravity_vector());

  builder.Connect(plant.get_body_poses_output_port(),
                  imu->get_input_port_body_poses());
  builder.Connect(plant.get_body_spatial_velocities_output_port(),
                  imu->get_input_port_body_velocities());
  builder.Connect(plant.get_body_spatial_accelerations_output_port(),
                  imu->get_input_port_body_accelerations());

  drake::geometry::MeshcatVisualizerParams params;
  params.publish_period = 1.0 / 60.0; // 60hz update rate
  auto meshcat = std::make_shared<drake::geometry::Meshcat>();
  drake::geometry::MeshcatVisualizer<double>::AddToBuilder(
      &builder, scene_graph, meshcat, std::move(params));

  auto array_input = builder.AddSystem<ArrayInputSystem>(cmd_vel_);
  builder.Connect(array_input->get_output_port(),
                  plant.get_actuation_input_port(model_instance));

  // Add a Drake visualiser instance to the diagram
  drake::geometry::DrakeVisualizer<double>::AddToBuilder(&builder, scene_graph);

  const std::unordered_set<drake::systems::TriggerType> kDefaultPublishTriggers{
      drake::systems::TriggerType::kForced,
      drake::systems::TriggerType::kPeriodic};

  auto imu_pub =
      builder.AddSystem(RosPublisherSystem::Make<sensor_msgs::msg::Imu>(
          "/imu/out", rclcpp::QoS(1), ros_interface_system->get_ros_interface(),
          kDefaultPublishTriggers, 1 / 100.0));

  auto imu_translator = builder.AddSystem<RosQuaternionConverter<double>>();
  builder.Connect(imu->get_output_port_measurement_quaternion_orientation(),
                  imu_translator->get_input_port());

  builder.Connect(imu_translator->get_output_port(), imu_pub->get_input_port());

  auto js_pub =
      builder.AddSystem(RosPublisherSystem::Make<sensor_msgs::msg::JointState>(
          "/joint_states", rclcpp::QoS(1),
          ros_interface_system->get_ros_interface(), kDefaultPublishTriggers,
          1 / 100.0));

  std::vector<std::string> target_joints = {"left_wheel_joint", "right_wheel_joint"};
  auto js_translator =
    builder.AddSystem<JointStatePublisherConverter<double>>(plant, target_joints);

  auto t = plant.GetStateNames();
  auto w = plant.GetVelocityNames();
  builder.Connect(plant.get_state_output_port(),
                  js_translator->get_input_port());

  builder.Connect(js_translator->get_output_port(), js_pub->get_input_port());

  // Build the complete system from the diagram
  auto diagram = builder.Build();

  // Create a simulator for the system
  auto simulator = std::make_unique<Simulator<double>>(*diagram);
  simulator->Initialize();

  auto &simulator_context = simulator->get_mutable_context();

  simulator->set_target_realtime_rate(1.0);

  // Step the simulator in 0.1s intervals
  constexpr double kStep{0.1};
  while (simulator_context.get_time() < FLAGS_simulation_sec) {

    rclcpp::spin_some(direct_ros_node);
    const double next_time =
        std::min(FLAGS_simulation_sec, simulator_context.get_time() + kStep);
    simulator->AdvanceTo(next_time);
  }

  return 0;
}
