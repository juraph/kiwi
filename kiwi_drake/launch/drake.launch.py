#!/usr/bin/env python3

import os
import xacro

from ament_index_python.packages import get_package_share_directory

from launch.actions import (
    IncludeLaunchDescription,
    DeclareLaunchArgument,
)

from launch import LaunchDescription
from launch_ros.actions import Node
from launch.launch_description_sources import PythonLaunchDescriptionSource


def generate_launch_description():

    # Get the robot description
    urdf_path = os.path.join(
        get_package_share_directory("kiwi_gazebo"), "urdf", "kiwi.xacro.urdf"
    )
    urdf_doc = xacro.parse(open(urdf_path, "r"))
    xacro.process_doc(urdf_doc)
    robot_description = urdf_doc.toxml()

    drake = Node(
        package="kiwi_drake",
        executable="kiwi_drake",
        name="kiwi_drake",
    )

    effort_test_node = Node(
        package="kiwi_controllers",
        executable="test_controller",
        output="screen",
    )

    pid_base_controller = Node(
        package="kiwi_controllers",
        executable="pid_base_controller",
        output="screen",
    )

    lqr_base_controller = Node(
        package="kiwi_controllers",
        executable="lqr_base_controller",
        output="screen",
    )

    mpc_base_controller = Node(
        package="kiwi_controllers",
        executable="mpc_base_controller",
        output="screen",
    )

    node_robot_state_publisher = Node(
        package="robot_state_publisher",
        executable="robot_state_publisher",
        output="screen",
        parameters=[{"robot_description": robot_description}],
    )

    hardware_package_location = get_package_share_directory("kiwi_control")
    hardware_interface = IncludeLaunchDescription(
        PythonLaunchDescriptionSource([hardware_package_location, '/launch/hardware_interface.launch.py']))


    return LaunchDescription(
        [
            DeclareLaunchArgument("verbose", default_value="false", description="verbose"),
            DeclareLaunchArgument("pause", default_value="false", description="paused"),
            node_robot_state_publisher,
            lqr_base_controller,
            # effort_test_node,
            drake,
        ]
    )
