#!/usr/bin/env bash

. /home/kiwi/kiwi_deps/install/setup.bash
. /home/kiwi/kiwi_ws/install/setup.bash
export GAZEBO_MASTER_URI=http://127.0.0.2:11345
ros2 launch kiwi_gazebo kiwi_sim.launch.py 
