#! /bin/sh
cd /home/kiwi/kiwi_ws/
rm -r build/*
rm -r install/*
cd src
rm -r kiwi_cad/meshes/*
cd /home/kiwi/kiwi_ws/src/kiwi_cad/cq && poetry install
cd /home/kiwi/kiwi_ws/src/scripts
./compile.sh
