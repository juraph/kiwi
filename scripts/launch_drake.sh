#!/usr/bin/env bash

. /home/kiwi/kiwi_deps/install/setup.bash
. /home/kiwi/kiwi_ws/install/setup.bash
ros2 launch kiwi_drake drake.launch.py
