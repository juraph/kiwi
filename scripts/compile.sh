#! /bin/bash
b3d_dir="../kiwi_cad/cq"
arch=$(uname -m)

build_develop(){
    cd /home/kiwi/kiwi_deps/
    colcon build --cmake-args -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_BUILD_TYPE=Release --symlink-install
    . /home/kiwi/kiwi_deps/install/setup.bash

    cd /home/kiwi/kiwi_ws/
    colcon build --cmake-args -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_BUILD_TYPE=Debug --symlink-install
    colcon test --packages-select kiwi_control

    cp /home/kiwi/kiwi_ws/build/compile_commands.json /home/kiwi/kiwi_ws/src/
}

build_hw(){
    cd /home/kiwi/kiwi_ws/kiwi_gazebo
    touch COLCON_IGNORE
    cd /home/kiwi/kiwi_ws/
    colcon build --cmake-args -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_BUILD_TYPE=Debug --symlink-install
    colcon test --packages-select kiwi_control
}


if [[  $arch == "aarch64" ]]; then
    cd "${b3d_dir}"
    mkdir ../meshes
    echo "RPI Detected, building hw system"
    build_hw
else
cd "${b3d_dir}"
if [ -z "$(ls -A ../meshes)" ]; then
    echo "No meshes found, building KIWI stl's!"
    poetry run build_stls
else
    echo "Meshes already exist, skipping CQ compilation"
fi
    build_develop
fi
