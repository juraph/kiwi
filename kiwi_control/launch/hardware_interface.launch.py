#!/usr/bin/env python3
import os
import xacro

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import IncludeLaunchDescription, ExecuteProcess
from launch.substitutions import LaunchConfiguration
from launch.substitutions import ThisLaunchFileDir
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch_ros.actions import Node


def generate_launch_description():
    hardware_package_location = get_package_share_directory("kiwi_control")

    # Get the launch configuration
    use_sim_time = LaunchConfiguration("use_sim_time", default="true")

    # Get the robot description
    urdf_path = os.path.join(
        get_package_share_directory("kiwi_gazebo"), "urdf", "kiwi.xacro.urdf"
    )
    urdf_doc = xacro.parse(open(urdf_path, "r"))
    xacro.process_doc(urdf_doc)
    robot_description = urdf_doc.toxml()

    robot_controller_config = os.path.join(
        get_package_share_directory("kiwi_control"),
        "config",
        "controllers.yaml",
    )

    hardware_config = os.path.join(hardware_package_location, "config", "hardware.yaml")

    robot_state_publisher = Node(
        name='robot_state_publisher',
        package='robot_state_publisher',
        executable='robot_state_publisher',
        parameters=[{'robot_description': robot_description}])


    joint_state_publisher = Node(
        package='joint_state_broadcaster',
        executable='joint_state_broadcaster',
        parameters=[{"robot_description": robot_description}],
        )


    joint_state_broadcaster_spawner = Node(
        package="controller_manager",
        executable="spawner",
        arguments=["joint_state_broadcaster", "--controller-manager", "/controller_manager"],
    )

    # Use the ROS controller manager to manage the controllers listed in hardware.yaml
    controller_manager = Node(
        package="controller_manager",
        executable="ros2_control_node",
        parameters=[{"robot_description": robot_description},
                    robot_controller_config,
                    ],
        output="screen",
        arguments=['--ros-args', '--log-level', 'info'],
    )

    effort_controller_spawner = Node(
        package="controller_manager",
        executable="spawner",
        arguments=["effort_controllers", "--controller-manager", "/controller_manager"],
    )

    keyboard_controller = Node(
        package="kiwi_controllers",
        executable="keyboard_control_node",
        # arguments=["effort_controllers", "--controller-manager", "/controller_manager"],
    )



    return LaunchDescription(
        [
            # controller_manager, # TODO: Re-implement this for the real hardware
            robot_state_publisher,
            joint_state_broadcaster_spawner,
            effort_controller_spawner,
            # keyboard_controller,
        ]
    )
