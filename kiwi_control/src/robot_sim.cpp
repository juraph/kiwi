#include "kiwi_control/robot_sim.hpp"

#include "rclcpp/duration.hpp"

#include <cassert>
#include <mutex>

#include <rclcpp/logging.hpp>

namespace kiwi_control {

RobotSim::RobotSim() { motor_command_.fill(0.0);
}

RobotSim::~RobotSim() {}

void RobotSim::write(const std::array<double, 2> &efforts) {
  std::lock_guard<std::mutex> lock(write_mutex_);
  motor_command_ = efforts;
}

KiwiState RobotSim::read() {
  std::lock_guard<std::mutex> lock(read_mutex_);
  return {current_state_};
}


} // namespace kiwi_control
