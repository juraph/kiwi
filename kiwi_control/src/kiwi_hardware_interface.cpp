#include "kiwi_control/kiwi_hardware_interface.hpp"
#include "hardware_interface/actuator_interface.hpp"

#include "rclcpp/logger.hpp"
#include "rclcpp/logging.hpp"

#include <hardware_interface/handle.hpp>
#include <hardware_interface/hardware_info.hpp>
#include <hardware_interface/system_interface.hpp>
#include <hardware_interface/types/hardware_interface_return_values.hpp>
#include <hardware_interface/types/hardware_interface_type_values.hpp>

namespace kiwi_control {

CallbackReturn
KiwiHardwareInterface::on_init(const hardware_interface::HardwareInfo &info) {
  RCLCPP_INFO(rclcpp::get_logger("hwi"), "OnInit called");
  if (info.name == "hardware_real") {
    // Not implemented currently
    // robot_.reset(new RobotBase);
  } else {
  }
  RCLCPP_INFO(rclcpp::get_logger("hwi"), "selected robotsim");
  if (hardware_interface::SystemInterface::on_init(info) !=
      CallbackReturn::SUCCESS) {
    std::cerr
        << "[KiwiHardwareInterface] Failed to initialise system interface!\n";
    return CallbackReturn::ERROR;
  }

  info_ = info;
  for (const auto &joint : info_.joints) {
    RCLCPP_INFO(rclcpp::get_logger("hwi"), "Joint: %s", joint.name.c_str());
  }

  return CallbackReturn::SUCCESS;
}

CallbackReturn
KiwiHardwareInterface::on_activate(const rclcpp_lifecycle::State &) {
  std::cout << "[Hardware interface] activated\n";
  return CallbackReturn::SUCCESS;
}

CallbackReturn
KiwiHardwareInterface::on_deactivate(const rclcpp_lifecycle::State &) {
  return CallbackReturn::SUCCESS;
}

hardware_interface::return_type
KiwiHardwareInterface::read(const rclcpp::Time &, const rclcpp::Duration &) {

  const auto current_state = robot_->read();
  return hardware_interface::return_type::OK;
}

hardware_interface::return_type
KiwiHardwareInterface::write(const rclcpp::Time &, const rclcpp::Duration &) {

  // Check to see if any of the commands are invalid
  if (std::any_of(
          hw_commands_.begin(), hw_commands_.end(),
          [](const double &command) { return !std::isfinite(command); })) {
    return hardware_interface::return_type::ERROR;
  }

  robot_->write(hw_commands_);
  return hardware_interface::return_type::OK;
}

std::vector<hardware_interface::StateInterface>
KiwiHardwareInterface::export_state_interfaces() {
  RCLCPP_INFO(rclcpp::get_logger("hwi"), "HWI exporting state interfaces!!");
  std::vector<hardware_interface::StateInterface> state_interfaces;
  for (size_t i = 0; i < info_.joints.size(); i++) {
    state_interfaces.emplace_back(hardware_interface::StateInterface(
        info_.joints[i].name, hardware_interface::HW_IF_POSITION,
        &hw_positions_.at(i)));
    state_interfaces.emplace_back(hardware_interface::StateInterface(
        info_.joints[i].name, hardware_interface::HW_IF_VELOCITY,
        &hw_velocities_.at(i)));
    state_interfaces.emplace_back(hardware_interface::StateInterface(
        info_.joints[i].name, hardware_interface::HW_IF_EFFORT,
        &hw_efforts_.at(i)));
  }
  return state_interfaces;
}
std::vector<hardware_interface::CommandInterface>
KiwiHardwareInterface::export_command_interfaces() {
  RCLCPP_INFO(rclcpp::get_logger("HardwareInter"), "HWI exporting command interaces!!!!");

  std::vector<hardware_interface::CommandInterface> command_interfaces;
  command_interfaces.reserve(info_.joints.size());
  for (size_t i = 0; i < info_.joints.size(); i++) {
    command_interfaces.emplace_back(hardware_interface::CommandInterface(
        info_.joints[i].name, hardware_interface::HW_IF_EFFORT,
        &hw_commands_.at(i)));
  }
  return command_interfaces;
}

} // namespace kiwi_control

#include "pluginlib/class_list_macros.hpp"
PLUGINLIB_EXPORT_CLASS(kiwi_control::KiwiHardwareInterface,
                       hardware_interface::SystemInterface)
