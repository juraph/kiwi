#ifndef ROBOT_BASE_H_
#define ROBOT_BASE_H_

#include "rclcpp_lifecycle/state.hpp"
#include <array>
#include <atomic>
#include <iostream>
#include <memory>
#include <mutex>
#include <string>
#include <thread>

namespace kiwi_control {

class KiwiState {
public:
  KiwiState() { std::cout << "state initialised\n"; }
  ~KiwiState() {}

  const KiwiState getState();

  std::array<double, 3> latest_state_;
};

class RobotBase {
public:
  RobotBase() {}
  ~RobotBase() {}

  virtual kiwi_control::KiwiState read() = 0;

  virtual void write(const std::array<double, 2> &efforts) = 0;

  KiwiState current_state_;
};

} // namespace kiwi_control

#endif // ROBOT_BASE_HPP_
