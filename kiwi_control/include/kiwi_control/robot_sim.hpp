#ifndef ROBOT_SIM_HPP_
#define ROBOT_SIM_HPP_


#include "kiwi_control/robot_base.hpp"

namespace kiwi_control {


class RobotSim : public RobotBase {
public:
  RobotSim();
  ~RobotSim();

  void initializeContinuousReading();

  void stopRobot();

  kiwi_control::KiwiState read();

  void write(const std::array<double, 2> &efforts);

private:
  std::mutex read_mutex_;
  std::mutex write_mutex_;
  std::array<double, 2> motor_command_;
};
} // namespace kiwi_control


#endif // ROBOT_SIM_HPP_
