

#include "kiwi_control/robot_base.hpp"

namespace kiwi_control {

// State of the kiwi robot.

class Robot : public RobotBase {
public:
  Robot();
  ~Robot();

  virtual kiwi_control::KiwiState read() = 0;

  virtual void write(const std::array<double, 2> &efforts) = 0;

private:
  std::mutex read_mutex_;
  std::mutex write_mutex_;
  std::array<double, 2> motor_command_;
};
} // namespace kiwi_control
