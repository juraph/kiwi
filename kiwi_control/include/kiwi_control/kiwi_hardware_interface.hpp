#ifndef KIWI_CONTROL__KIWI_HARDWARE_INTERFACE_H_
#define KIWI_CONTROL__KIWI_HARDWARE_INTERFACE_H_

#include "rclcpp_lifecycle/node_interfaces/lifecycle_node_interface.hpp"
#include <rclcpp/rclcpp.hpp>

#include "kiwi_control/robot.hpp"
#include "kiwi_control/robot_base.hpp"

#include <hardware_interface/actuator_interface.hpp>
#include <hardware_interface/system_interface.hpp>

using CallbackReturn = rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn;

namespace kiwi_control {

class KiwiHardwareInterface : public hardware_interface::SystemInterface {
public:
  CallbackReturn
  on_init(const hardware_interface::HardwareInfo &system_info) override;

  CallbackReturn
  on_activate(const rclcpp_lifecycle::State &previous_state) override;

  CallbackReturn
  on_deactivate(const rclcpp_lifecycle::State &previous_state) override;

  hardware_interface::return_type read(const rclcpp::Time & time, const rclcpp::Duration & period) override ;

  hardware_interface::return_type write(const rclcpp::Time & time, const rclcpp::Duration & period) override;

  std::vector<hardware_interface::StateInterface>
  export_state_interfaces();

  std::vector<hardware_interface::CommandInterface>
  export_command_interfaces();

private:
  std::unique_ptr<RobotBase> robot_;

  std::array<double, 2> hw_efforts_{0, 0};
  std::array<double, 2> hw_commands_{0, 0};
  std::array<double, 2> hw_positions_{0, 0};
  std::array<double, 2> hw_velocities_{0, 0};
};

} // namespace kiwi_control
#endif // KIWI_CONTROL__KIWI_HARDWARE_INTERFACE_H_
