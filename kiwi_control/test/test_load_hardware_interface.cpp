
#include <exception>
#include <gmock/gmock.h>

#include <cmath>
#include <string>
#include <unordered_map>
#include <vector>

#include <hardware_interface/loaned_command_interface.hpp>
#include <hardware_interface/loaned_state_interface.hpp>
#include <hardware_interface/resource_manager.hpp>
#include <hardware_interface/types/lifecycle_state_names.hpp>
#include <lifecycle_msgs/msg/state.hpp>
#include <rclcpp/rclcpp.hpp>
#include <rclcpp_lifecycle/state.hpp>
#include <ros2_control_test_assets/components_urdfs.hpp>
#include <ros2_control_test_assets/descriptions.hpp>

#include "kiwi_control/kiwi_hardware_interface.hpp"

class TestKiwiHardwareInterface : public ::testing::Test {
protected:
  void SetUp() override {
    hardware_kiwi_ =
        R"(
  <ros2_control name="test_kiwi_hardware" type="system">
    <hardware>
      <plugin>kiwi_control/KiwiHardwareInterface</plugin>
    </hardware>
  </ros2_control>
)";
  }

  std::string hardware_kiwi_;
};

TEST_F(TestKiwiHardwareInterface, load_kiwi_base_interface) {
  auto urdf = ros2_control_test_assets::urdf_head + hardware_kiwi_ +
              ros2_control_test_assets::urdf_tail;
  try {
    hardware_interface::ResourceManager rm(urdf);
  } catch (const std::exception &err) {
    // check exception
    ASSERT_STREQ("error message", err.what());
  }
}
