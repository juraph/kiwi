#ifndef KIWI_BASE__KIWI_BASE_HPP_
#define KIWI_BASE__KIWI_BASE_HPP_

#include <signal.h>

#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include <rclcpp/rclcpp.hpp>

/*
 Entry point for KIWI
*/
int main(int argc, char *argv[]);

/// Handler for ctrl+c interrupts
void intHandler(int dum);

#endif // KIWI_BASE__KIWI_BASE_HPP_
