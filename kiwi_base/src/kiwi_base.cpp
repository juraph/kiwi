#include "kiwi_base/kiwi_base.hpp"

void intHandler(int dum) {

  // int test = controller_->shutdown().id()
  // if (controller_->shutdown().id() !=
  //     lifecycle_msgs::msg::State::PRIMARY_STATE_FINALIZED)
  // {
  //   throw std::runtime_error("Could not shutdown the controller!");
  // }

  rclcpp::shutdown();

  exit(0);
}

// Entry point for KIWI system
int main(int argc, char *argv[]) {
  // force flush of the stdout buffer.
  // this ensures a correct sync of all prints
  // even when executed simultaneously within the launch file.
  setvbuf(stdout, NULL, _IONBF, BUFSIZ);
  // Set up handle to handle ctrl+c killing of software
  signal(SIGINT, intHandler);
  rclcpp::init(argc, argv);

  rclcpp::executors::SingleThreadedExecutor exe;
  rclcpp::NodeOptions controller_options;

  exe.spin();

  rclcpp::shutdown();

  return 0;
}
