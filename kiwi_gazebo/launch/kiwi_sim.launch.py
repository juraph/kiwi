#!/usr/bin/env python3

import os

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch.actions import (
    ExecuteProcess,
    IncludeLaunchDescription,
    RegisterEventHandler,
    DeclareLaunchArgument,
)
from launch.event_handlers import OnProcessExit
from launch.launch_description_sources import PythonLaunchDescriptionSource

import xacro
from launch_ros.actions import Node


def generate_launch_description():
    gazebo = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            [
                os.path.join(get_package_share_directory("gazebo_ros"),
                             "launch"), "/gazebo.launch.py",
            ]
        ),
    )

    pkg_gazebo_ros = get_package_share_directory("gazebo_ros")
    # Get the robot description
    urdf_path = os.path.join(
        get_package_share_directory("kiwi_gazebo"), "urdf", "kiwi.xacro.urdf"
    )
    urdf_doc = xacro.parse(open(urdf_path, "r"))
    xacro.process_doc(urdf_doc)
    robot_description = urdf_doc.toxml()

    effort_test_node = Node(
        package="kiwi_controllers",
        executable="test_controller",
        output="screen",
    )

    pid_base_controller = Node(
        package="kiwi_controllers",
        executable="pid_base_controller",
        output="screen",
    )

    lqr_base_controller = Node(
        package="kiwi_controllers",
        executable="lqr_base_controller",
        output="screen",
    )

    node_robot_state_publisher = Node(
        package="robot_state_publisher",
        executable="robot_state_publisher",
        output="screen",
        parameters=[{"robot_description": robot_description}],
    )

    spawn_kiwi = Node(
        package="gazebo_ros",
        executable="spawn_entity.py",
        arguments=["-topic", "robot_description", "-entity", "kiwi", "-z", "0.15"],
        output="screen",
    )

    hardware_package_location = get_package_share_directory("kiwi_control")
    hardware_interface = IncludeLaunchDescription(
        PythonLaunchDescriptionSource([hardware_package_location, '/launch/hardware_interface.launch.py']))


    return LaunchDescription(
        [
            DeclareLaunchArgument(
                "verbose", default_value="false", description="verbose"
            ),
            DeclareLaunchArgument("pause", default_value="false", description="paused"),
            # node_robot_state_publisher,
            gazebo,
            spawn_kiwi,
            hardware_interface,
            lqr_base_controller,
            # effort_test_node,
        ]
    )
