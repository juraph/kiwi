#!/usr/bin/env python3

import os

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch.actions import (
    IncludeLaunchDescription,
    DeclareLaunchArgument,
)
from launch.launch_description_sources import PythonLaunchDescriptionSource


def generate_launch_description():
    gazebo = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            [
                os.path.join(get_package_share_directory("gazebo_ros"), "launch"),
                "/gazebo.launch.py",
            ]
        ),
    )

    pkg_gazebo_ros = get_package_share_directory("gazebo_ros")

    return LaunchDescription(
        [
            DeclareLaunchArgument(
                "world",
                default_value=[
                    os.path.join(pkg_gazebo_ros, "worlds", "empty.world"),
                    "",
                ],
                description="SDF world file",
            ),
            DeclareLaunchArgument(
                "verbose", default_value="false", description="verbose"
            ),
            DeclareLaunchArgument("pause", default_value="true", description="paused"),
            gazebo,
        ]
    )
